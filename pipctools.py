#!/usr/bin/python3

import os
import sys
import time
from struct import *
from urllib.request import Request, urlopen
from urllib.error import URLError, HTTPError
from xml.dom import minidom


class Pipcontroller:

    def __check_proc(self, procname):
        pids = [pid for pid in os.listdir('/proc') if pid.isdigit()]
        for pid in pids:
            try:
                proc = open("/proc/" + pid + "/cmdline", "rb").read().decode('ascii')
                if proc.find(procname) > -1:
                    return True
            except IOError:  # proc has already terminated
                continue
        return False

    def cmd(self, cmd):
        os.system("echo " + cmd + " > /pipc/oset")

    def start(self):
        if self.pipcontroller_running():
            print("pipcontroller already running")
        else:
            print("starting pipcontroller")
            os.system("/pipc/pipcontroller &")

    def stop(self):
        if self.pipcontroller_running():
            print("Stopping pipcontroller")
            self.cmd("exit")
        else:
            print("Pipcontroller already stopped")

    # @val: 0,1
    def setdo(self, idx, val):
        self.cmd("SETDO " + str(idx) + " " + str(val))

    # @val: 0,1
    def setdos(self, val):
        self.cmd("SETDO all " + str(val))

    # @val: 0...65535
    def setao(self, idx, val):
        self.cmd("SETAO " + str(idx) + " " + str(val))

    def setaos(self, val):
        self.cmd("SETAO all " + str(val))

    def pipcontroller_running(self):
        return self.__check_proc("pipcontroller")

    def pipwatchdog_running(self):
        return self.__check_proc("pipwatchdog.sh")

    def kill_pipwatchdog(self):
        if self.pipwatchdog_running():
            os.system("killall -9 pipwatchdog.sh")

    def start_pipwatchdog(self):
        if not self.pipwatchdog_running():
            os.system("/pipc/pipwatchdog.sh &")

    def broadcastProcRunning(self):
        return self.__check_proc("pipcbcast")

    def kill_broadcast(self):
        if self.broadcastProcRunning():
            os.system("killall -9 pipcbcast")

    def start_broadcast(self):
        if not self.broadcastProcRunning():
            os.system("/pipc/pipcbcast &")

    def pipcfrontProcRunning(self):
        return self.__check_proc("pipfront")

    def start_pipfront(self):
        if not self.pipcfrontProcRunning():
            os.system("/pipc/pipfront &")

    def kill_pipfront(self):
        if self.pipcfrontProcRunning():
            os.system("killall -9 pipfront")

    def status(self, url="http://127.0.0.1", port=43900):

        if not self.pipcontroller_running():
            return "controller not running"

        try:
            url_str = url + ':' + str(port) + "/statusall"
            xml_str = urlopen(url_str).read()
            xml_doc = minidom.parseString(xml_str)
            return xml_doc.toprettyxml(indent="\t")
        except URLError as e:
            return "URLError "

        except HTTPError as e:
            return "HTTPError"

        except Exception:
            return "Error"

    def usrsoload(self):
        self.cmd("usrsoload")

    def usrsounload(self):
        self.cmd("usrsounload")

    # @debug: 0/1
    def usrsodebug(self, debug):
        self.cmd("usrsodebug " + str(debug))

    def get_serial(self):

        try:
            f = open("/pipc/sn.bin", "rb")
            sn = f.read(8)
            un = unpack('II', sn)
            f.close()
            return un[1]
        except Exception:
            return 0

        return 0

    def get_file_paths(self):
        app_path = "/pipc/app_tmp"
        os.chdir(app_path)
        file_paths = []
        for subdir, dirs, files in os.walk(app_path):
            for file in files:
                filepath = subdir + os.sep + file
                file_paths.append(filepath)
        return file_paths

    def updatePipc(self, file_list):

        os.makedirs("/pipc", exist_ok=True)
        os.makedirs("/pipc/tools", exist_ok=True)
        os.makedirs("/pipc/mail", exist_ok=True)
        os.system("sudo chmod 777 -R /pipc")

        for file_long in file_list:

            file = os.path.basename(file_long)

            if file == "pipcbcast":
                self.kill_broadcast()
                os.rename(file_long, "/pipc/" + file)
                os.system("sudo chmod 775 /pipc/pipcbcast")
                self.start_broadcast()
            elif file == "pipfront":
                # print("HHHHHHHHHHHH")
                # dest_file = "/pipc/" + file_short
                # print(dest_file)
                self.kill_pipfront()
                os.rename(file_long, "/pipc/" + file)
                os.system("sudo chmod 775 /pipc/pipfront")
                self.start_pipfront()
            elif file == "pipwatchdog.sh":
                self.kill_pipwatchdog()
                os.rename(file_long, "/pipc/" + file)
                os.system("sudo chmod 775 /pipc/pipwatchdog.sh")
                self.start_pipwatchdog()
            elif file == "pip":
                os.rename(file_long, "/etc/init.d/pip")
                os.system("sudo chmod 775 /etc/init.d/pip")
            elif file == "pipcontroller":
                self.stop()
                start_time = time.time()
                while self.pipcontroller_running():
                    time.sleep(1)
                    now = time.time()
                    if now > start_time + 15:
                        os.system("killall -9 pipcontroller")
                        time.sleep(2)
                        break
                os.rename(file_long, "/pipc/" + file)
                os.system("sudo chmod 775 /pipc/pipcontroller")
                self.start()

            elif file == "i2c":
                os.rename(file_long, "/pipc/tools/i2c")
                os.system("sudo chmod 775 /pipc/tools/i2c")
            elif file == "i2c_detect_loop":
                os.rename(file_long, "/pipc/tools/i2c_detect_loop")
                os.system("sudo chmod 775 /pipc/tools/i2c_detect_loop")
            elif file == "w1check.sh":
                os.rename(file_long, "/pipc/tools/w1check.sh")
                os.system("sudo chmod 775 /pipc/tools/w1check.sh")
            elif file == "maillist":
                os.rename(file_long, "/pipc/mail/maillist")
            elif file == "mailconf":
                os.rename(file_long, "/pipc/mail/mailconf")
            elif file == "pipcmail":
                os.rename(file_long, "/pipc/mail/pipcmail")
                os.system("sudo chmod 775 /pipc/mail/pipcmail")
            else:
                print("Unknown file '" + file + "'")


# Examples:
# pc = Pipcontroller()
# pc.setdo(31, 0)
# print(pc.status())
# pc.stop()
# pc.start()
# print(pc.get_serial())
# pc.start_pipwatchdog()
# pc.kill_pipwatchdog()


class LinuxTools:

    def localtimeFullPath(self):
        if not os.path.islink("/etc/localtime"):
            return "not_symlink"

        ret = os.popen("ls -la /etc/localtime").read()
        # print(ret)

        split = ret.split("-> ", 1)
        if len(split) < 2:
            return "cannot_extract_link"

        return split[1]
        # return os.path.realpath("/etc/localtime");

    def localtime(self):
        path = self.localtimeFullPath()
        return os.path.basename(path)

    # @localtime: etc "Ljubljana"
    # @return: False on failure, True on success
    def setLocaltimeEurope(self, localtime):

        ref = "/usr/share/zoneinfo/Europe/" + localtime

        if not os.path.exists(ref):
            sys.stderr.write("Cannot set localtime '" + localtime + "'")
            return False

        if os.system("mv /etc/localtime /etc/localtime_old"):
            return False
        if os.system("ln -s " + ref + " /etc/localtime"):
            return False

        return True

    def getCitiesEurope(self, sort=True):

        cities = os.listdir('/usr/share/zoneinfo/Europe/')
        if sort:
            return sorted(cities)
        else:
            return cities


# Examples:
# ut = LinuxTools()
# ut.localtime()
# ut.setLocaltimeEurope("Zagreb")
# print(ut.getCitiesEurope())
