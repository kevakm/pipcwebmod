import json

from django.template.defaulttags import register


@register.filter
def get_value(dictionary, key):
    dict = json.loads(dictionary)
    return dict.get(key)


@register.filter
def get_nested_value(dictionary, args):
    if args is None:
        return False
    arg_list = [arg.strip() for arg in args.split(',')]

    dict = json.loads(dictionary)
    id = dict.get(arg_list[0])
    value = id[arg_list[1]]
    return value


@register.filter
def to_class_name(object):
    return object.__class__.__name__


@register.filter
def tcp_type(queryset, type):
    return queryset.filter(type=type)
