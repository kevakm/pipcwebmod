import json
import os
import zipfile
from urllib.request import Request
from urllib.request import urlopen

try:
    from app.models import ProgressBar
except:
    pass


def downloadApp(BASE_DIR, write_to_base=True):

    base_dir = os.path.dirname(BASE_DIR)
    os.chdir(base_dir)

    url = "https://panel.i13tech.com/firmware/get.php?API-KEY=c4MctNk+92T1GtL2ICaTxwZMBteQtE9moK6qcHVC"
    a = {"app_download_request": "pipc", "get_app_info": False}
    app_data = json.dumps(a).encode('utf-8')
    app = Request(url, app_data, {'Content-Type': 'application/json'})

    app_path = os.path.join(base_dir, 'app_tmp')
    app_zipath = os.path.join(base_dir, 'app_copy.zip')

    try:
        app_response = urlopen(app, timeout=30)
        file_size = app_response.getheader('Content-Length')

        output = open(app_zipath, "wb")
        chunk = 128000
        percentage = 0
        while 1:
            if write_to_base is True:
                object = ProgressBar.objects.all().first()
            data = output.write(app_response.read(chunk))
            if not data:
                print("Total bytes downloaded: {}".format(file_size))
                if write_to_base is True:
                    object.percentage = 100
                    object.save()
                output.close()
                break
            percentage += (data / int(file_size)) * 100
            if write_to_base is True:
                object.percentage = int(percentage)
                object.save()
            print("Zipping: {}".format(int(percentage)))

        zip = zipfile.ZipFile(app_zipath)
        zip.extractall(app_path)
        zip.close()
        os.remove(app_zipath)
        os.system("sudo chmod 777 -R " + app_path)
    except:
        if write_to_base is True:
            object = ProgressBar.objects.all().first()
            object.percentage = 999
            object.save()


def get_app_gui_version():
    url = "https://panel.i13tech.com/firmware/get.php?API-KEY=c4MctNk+92T1GtL2ICaTxwZMBteQtE9moK6qcHVC"
    a = {"app_download_request": "pipcwebmod", "get_app_info": True}
    info_data = json.dumps(a).encode('utf-8')
    req = Request(url, info_data, {'Content-Type': 'application/json'})
    response = urlopen(req)
    respo = response.read().decode('utf-8')
    resp = json.loads(respo)
    gui_ver = resp['gui_ver']
    app_ver = resp['app_ver']
    return [gui_ver, app_ver]
