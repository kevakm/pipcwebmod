from app.models import ModbusServer, ModbusDevice, ModbusCard, Modbus


def modbuss_to_ini(enable, read_only, port):
    ModbusServer.objects.all().delete()

    if enable == "on":
        enable = True
    else:
        enable = False
    if read_only == "on":
        read_only = True
    else:
        read_only = False
    if port == "":
        port = 502

    object = ModbusServer(enable=enable, read_only=read_only, port=port)
    object.save()


def save_modbus_device(object):
    id = object['id']
    name = object['name']
    ip_address = object['ip_address']
    port = object['port']
    slave_address = object['slave_address']

    device = ModbusDevice.objects.get(id=int(id))
    device.name = name
    device.ip_address = ip_address
    if port != "":
        device.port = int(port)
    device.slave_address = slave_address
    device.save()


def save_modbus_cards(cards):
    for card in cards:
        id = card['id']
        card_object = ModbusCard.objects.get(id=int(id))
        sen_db = card_object.modbus_set.all().count()
        sen = int(card['no_elements'])

        if sen_db <= sen:
            k = 0
            i = sen - sen_db
            e = sen_db
            for x in range(e):
                object = Modbus.objects.filter(card=card_object)[x]
                object.name = card['sensor_names'][x]
                object.type = card['type']
                # object.card = card_object
                object.save()
                k += 1
            for x in range(i):
                object = Modbus(name=card['sensor_names'][k], type=card['type'], card=card_object)
                object.save()
                k += 1
        elif sen_db > sen:
            i = sen_db - sen
            for x in range(i):
                Modbus.objects.filter(card=card_object).last().delete()

        card_object.name = card['name']
        card_object.type = card['type']
        card_object.registers = card['registers']
        card_object.update_time = int(card['update_time'])
        card_object.ini_policy = card['ini_policy']
        if card['address'] != "":
            card_object.address = card['address']
        card_object.no_elements = int(card['no_elements'])
        card_object.save()
