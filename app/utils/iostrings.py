import os

from pipcwebmod.settings import BASE_DIR, PIPC_DIR
from app.models import AnalogInput, DigitalInput, DigitalOutput, AnalogOutput, OneWire, VirtualDigitalOutput, \
    VirtualSensor, VirtualAnalogOutput


def update_iostrings():
    path = os.path.join(PIPC_DIR, 'dev', 'iostrings.txt')

    temp_sensors = AnalogInput.objects.filter(type=0)

    context = "[TSENSORS]"

    i = 0
    if temp_sensors:
        for sensor in temp_sensors:
            name = sensor.name
            id = int(i)
            i += 1
            context += "\n{}:{}".format(id, name)

    ows_sensors = OneWire.objects.all()
    ows_0sensors = OneWire.objects.filter(bus_id=0)
    ows_1sensors = OneWire.objects.filter(bus_id=1)

    if ows_sensors:
        if ows_1sensors:
            i = len(temp_sensors)
            for sensor in ows_1sensors:
                name = sensor.name
                id = int(i)
                i += 1
                context += "\n{}:{}".format(id, name)

        if ows_0sensors:
            for sensor in ows_0sensors:
                name = sensor.name
                id = int(i)
                i += 1
                context += "\n{}:{}".format(id, name)


    virtual_sensors = VirtualSensor.objects.all()

    if virtual_sensors:
        for sensor in virtual_sensors:
            name = sensor.name
            id = int(i)
            i += 1
            context += "\n{}:{}".format(id, name)
        context += "\n[/TSENSORS]\n"
    else:
        context += "\n[/TSENSORS]\n"

    digital_outputs = DigitalOutput.objects.all()

    context += "\n[DO]"
    if digital_outputs:
        i = 0
        for output in digital_outputs:
            name = output.name
            id = int(i)
            i += 1
            context += "\n{}:{}".format(id, name)

    virtual_digital_outputs = VirtualDigitalOutput.objects.all()

    if virtual_digital_outputs:
        i = len(digital_outputs)
        for output in virtual_digital_outputs:
            name = output.name
            id = int(i)
            i += 1
            context += "\n{}:{}".format(id, name)
        context += "\n[/DO]\n"
    else:
        context += "\n[/DO]\n"

    analog_outputs = AnalogOutput.objects.all()
    virtual_analog_outputs = VirtualAnalogOutput.objects.all()

    if analog_outputs:
        i = 0
        context += "\n[AO]"
        for output in analog_outputs:
            name = output.name
            id = int(i)
            i += 1
            context += "\n{}:{}".format(id, name)
        if virtual_analog_outputs:
            for output in virtual_analog_outputs:
                name = output.name
                id = int(i)
                i += 1
                context += "\n{}:{}".format(id, name)
        context += "\n[/AO]\n"
    elif virtual_analog_outputs:
        i = 0
        context += "\n[AO]"
        for output in virtual_analog_outputs:
            name = output.name
            id = int(i)
            i += 1
            context += "\n{}:{}".format(id, name)
        context += "\n[/AO]\n"

    digital_inputs = DigitalInput.objects.all()

    if digital_inputs:
        i = 0
        context += "\n[DI]"
        for input in digital_inputs:
            name = input.name
            id = int(i)
            i += 1
            context += "\n{}:{}".format(id, name)
        context += "\n[/DI]\n"

    analog_inputs = AnalogInput.objects.all()

    if analog_inputs:
        i = 0
        context += "\n[AI]"
        for input in analog_inputs:
            name = input.name
            id = int(i)
            i += 1
            context += "\n{}:{}".format(id, name)
        context += "\n[/AI]"

    with open(path, "w") as file:
        file.write(context)

    virtalio_path = os.path.join(PIPC_DIR, 'dev', 'virtualio.ini')
    T = len(virtual_sensors)
    DO = len(virtual_digital_outputs)
    AO = len(virtual_analog_outputs)

    with open(virtalio_path, "w") as file:
        file.write("T={}\nDO={}\nAO={}".format(T, DO, AO))

    if analog_inputs:
        adc_path = os.path.join(PIPC_DIR, 'dev', 'adcsensors.ini')
        ADC_context = ""
        i = 0
        for sensor in analog_inputs:
            type = sensor.get_type_display()
            ADC_context += "[ADC{}]\ntype={}\n[/ADC{}]\n\n".format(int(i), type, int(i))
            i += 1

        with open(adc_path, "w") as file:
            file.write(ADC_context)
