
def validate_first_char(line):
    if line[0] == "\n" or line[0] == "#" or line[0] == ";" or line[0] == "" or line[0] == " ":
        return False
    else:
        return True


def strip_strings_in_list(list):
    list = [string.split(":", 1)[1] for string in list]
    return list
