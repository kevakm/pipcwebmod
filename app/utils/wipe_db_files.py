import os

from pipcwebmod.settings import PIPC_DIR
from app.models import DigitalOutput, AnalogInput, AnalogOutput, DigitalInput, VirtualDigitalOutput, VirtualSensor, \
    VirtualAnalogOutput, OneWire, DomesticWater, ModbusServer, HeatingZone


def delete_files():
    path = os.path.join(PIPC_DIR)
    dev_files = ['1wire_0.ini', "1wire_1.ini", "adcsensors.ini", "iostrings.txt", "modbussrv.ini", "virtualio.ini"]
    ctrl_files = ['dhw.init.ini', 'hccontrol.ini']

    for file in dev_files:
        try:
            os.remove(os.path.join(path, 'dev', file))
        except:
            pass

    for file in ctrl_files:
        try:
            os.remove(os.path.join(path, 'ctrl_sett', file))
        except:
            pass


def delete_db():

    OneWire.objects.all().delete()
    DomesticWater.objects.all().delete()
    ModbusServer.objects.all().delete()
    DigitalOutput.objects.all().delete()
    AnalogInput.objects.all().delete()
    AnalogOutput.objects.all().delete()
    DigitalInput.objects.all().delete()
    VirtualDigitalOutput.objects.all().delete()
    VirtualSensor.objects.all().delete()
    VirtualAnalogOutput.objects.all().delete()
    HeatingZone.objects.all().delete()
