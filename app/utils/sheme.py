from app.models import DigitalOutput, AnalogInput, AnalogOutput, DigitalInput, VirtualDigitalOutput, VirtualSensor, \
    VirtualAnalogOutput, Modbus


def sheme_to_db(digital_outputs, analog_inputs, analog_outputs, digital_inputs, vdo, virtual_sensors, vao):

    i = 0
    for output in digital_outputs:
        object = DigitalOutput.objects.all()[i]
        object.name = output
        object.save()
        i += 1

    i = 0
    for input in analog_inputs:
        object = AnalogInput.objects.all()[i]
        object.name = input['name']
        if object.para_type != input['para_type']:
            object.parameters = input['parameters']
        object.para_type = input['para_type']
        object.type = input['type']
        object.save()
        i += 1

    i = 0
    for output in analog_outputs:
        object = AnalogOutput.objects.all()[i]
        object.name = output
        object.save()
        i += 1

    i = 0
    for input in digital_inputs:
        object = DigitalInput.objects.all()[i]
        object.name = input
        object.save()
        i += 1

    VDO_objects = VirtualDigitalOutput.objects.all()
    VDO_ids = [object.id for object in VDO_objects]

    for item in vdo:
        id = int(item['id'])

        if id in VDO_ids:
            VDO_ids.remove(id)
            object = VirtualDigitalOutput.objects.get(id=id)
            object.name = item['name']
            object.save()
        if id == -1:
            object = VirtualDigitalOutput(name=item['name'])
            object.save()

    for object_id in VDO_ids:
        try:
            VirtualDigitalOutput.objects.get(id=int(object_id)).delete()
        except:
            pass

    VS_objects = VirtualSensor.objects.all()
    VS_ids = [object.id for object in VS_objects]

    for item in virtual_sensors:
        id = int(item['id'])

        if id in VS_ids:
            VS_ids.remove(id)
            object = VirtualSensor.objects.get(id=id)
            object.name = item['name']
            object.save()
        if id == -1:
            object = VirtualSensor(name=item['name'])
            object.save()

    for object_id in VS_ids:
        try:
            VirtualSensor.objects.get(id=int(object_id)).delete()
        except:
            pass

    VAO_objects = VirtualAnalogOutput.objects.all()
    VAO_ids = [object.id for object in VAO_objects]

    for item in vao:
        id = int(item['id'])

        if id in VAO_ids:
            VAO_ids.remove(id)
            object = VirtualAnalogOutput.objects.get(id=id)
            object.name = item['name']
            object.save()
        if id == -1:
            object = VirtualAnalogOutput(name=item['name'])
            object.save()

    for object_id in VAO_ids:
        try:
            VirtualAnalogOutput.objects.get(id=int(object_id)).delete()
        except:
            pass


def tcp_to_modbus(tcps):

    objects = Modbus.objects.all()
    ids = [object.id for object in objects]

    for object in tcps:
        id = int(object['id'])

        if id in ids:
            ids.remove(id)
            o = Modbus.objects.get(id=id)
            o.name = object['name']
            o.type = object['type']
            o.save()
        if id == -1:
            o = Modbus(name=object['name'], type=object['type'])
            o.save()

    for id in ids:
        try:
            Modbus.objects.get(id=int(id)).delete()
        except:
            pass