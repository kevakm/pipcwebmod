import json
import os

from pipcwebmod.settings import PIPC_DIR, WIRE_DIR
from app.models import OneWire
from app.utils.read_all_files import validate_first_char


def get_objects(*args):
    owfs_objects = []
    ini_objects = []

    base_path = os.path.join(PIPC_DIR, 'dev')
    wire_zero_path = os.path.join(base_path, '1wire_0.ini')
    wire_one_path = os.path.join(base_path, '1wire_1.ini')

    if not os.path.exists(wire_zero_path):
        with open(wire_zero_path, "w") as f:
            f.write("")

    if not os.path.exists(wire_one_path):
        with open(wire_one_path, "w") as f:
            f.write("")

    for i in args:
        owfs_path = os.path.join(WIRE_DIR, 'bus.' + str(i))
        ini_path = os.path.join(PIPC_DIR, 'dev', '1wire_{}.ini'.format(str(i)))
        for filename in os.listdir(owfs_path):
            if len(filename) == 15 and filename[:2] == "28":
                info = {"address": filename, "address_ini": filename.replace('.', ''), "bus_id": i}
                owfs_objects.append(info)

        with open(ini_path, 'r') as file:
            content = []
            for line in file:
                if validate_first_char(line):
                    content.append(line)
            content = [x.strip() for x in content]
            if i == 0:
                bus_id = 1
            elif i == 1:
                bus_id = 0
            for row in content:
                info_list = row.split(" ")
                trim = 0.0
                name = ""
                for string in info_list:
                    if "trim" in string:
                        trim = string.split("=", 1)[1]

                    if "name" in string:
                        name = string.split("=", 1)[1]

                info = {"address": info_list[0][:2] + "." + info_list[0][2:], "address_ini": info_list[0],
                        "trim": trim, "name": name, "bus_id": bus_id}
                ini_objects.append(info)

    dict = {"owfs_sensors": owfs_objects, "onewire_ini": ini_objects}
    return json.dumps(dict)


def onewire_to_ini(py_dict):
    base_path = os.path.join(PIPC_DIR, 'dev')
    wire_zero_path = os.path.join(base_path, '1wire_0.ini')
    wire_one_path = os.path.join(base_path, '1wire_1.ini')
    with open(wire_zero_path, "w") as f:
        f.write("")
    with open(wire_one_path, "w") as f:
        f.write("")

    for ini_sensor in py_dict:
        address_ini = ini_sensor['address_ini']
        trim = ini_sensor['trim']
        name = ini_sensor['name']
        bus_id = ini_sensor['bus_id']

        if bus_id == 0:
            path = os.path.join(base_path, '1wire_1.ini')
            with open(path, "a") as file:
                file.write("{} trim={} name={}\n".format(address_ini, trim, name))
        elif bus_id == 1:
            path = os.path.join(base_path, '1wire_0.ini')
            with open(path, "a") as file:
                file.write("{} trim={} name={}\n".format(address_ini, trim, name))

        if OneWire.objects.filter(address=address_ini).exists():
            object = OneWire.objects.get(address=address_ini)
            object.trim = trim
            object.name = name
            object.bus_id = bus_id
            object.save()
        else:
            object = OneWire(address=address_ini, trim=trim, name=name, bus_id=bus_id)
            object.save()
