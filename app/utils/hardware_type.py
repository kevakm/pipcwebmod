import os

from app.models import AnalogInput, DigitalOutput, AnalogOutput, DigitalInput
from app.utils.read_all_files import validate_first_char
from pipcwebmod.settings import PIPC_DIR


def get_hardware_type():
    path = os.path.join(PIPC_DIR, 'dev', 'hwd_info.txt')
    print(path)
    print(os.path.exists(path))
    with open(path, "r") as file:
        content = []
        for line in file:
            if validate_first_char(line) == True:
                content.append(line)
        content = [x.strip() for x in content]

        for x in content:
            if "type" in x:
                type = x.split("=", 1)[1].strip().lower()

        return type


def correct_sheme(DO, AI, AO, DI):

    AI_db = AnalogInput.objects.all().count()
    DO_db = DigitalOutput.objects.all().count()
    AO_db = AnalogOutput.objects.all().count()
    DI_db = DigitalInput.objects.all().count()

    if AI_db < AI:
        i = AI - AI_db
        e = AI_db
        for x in range(i):
            object = AnalogInput(name="AI" + str(e))
            object.save()
            e += 1
    elif AI_db > AI:
        i = AI_db - AI
        for x in range(i):
            AnalogInput.objects.all().last().delete()

    if DO_db < DO:
        i = DO - DO_db
        e = DO_db
        for x in range(i):
            object = DigitalOutput(name="DO" + str(e))
            object.save()
            e += 1
    elif DO_db > DO:
        i = DO_db - DO
        for x in range(i):
            DigitalOutput.objects.all().last().delete()

    if AO_db < AO:
        i = AO - AO_db
        e = AO_db
        for x in range(i):
            object = AnalogOutput(name="AO" + str(e))
            object.save()
            e += 1
    elif AO_db > AO:
        i = AO_db - AO
        for x in range(i):
            AnalogOutput.objects.all().last().delete()

    if DI_db < DI:
        i = DI - DI_db
        e = DI_db
        for x in range(i):
            object = DigitalInput(name="DI" + str(e))
            object.save()
            e += 1
    elif DI_db > DI:
        i = DI_db - DI
        for x in range(i):
            DigitalInput.objects.all().last().delete()
