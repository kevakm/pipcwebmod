import json
import os
import shutil
import zipfile
from itertools import chain

import subprocess
from urllib.request import urlopen, Request

from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView
from django.views.generic import TemplateView

from app.utils.app_updater import downloadApp, get_app_gui_version
from app.utils.selected_dos import selected_dos
from pipctools import LinuxTools, Pipcontroller
from pipcwebmod.settings import BASE_DIR, GUI_VERSION
from app.models import DomesticWater, ModbusServer, DigitalOutput, AnalogInput, AnalogOutput, DigitalInput, \
    VirtualDigitalOutput, VirtualSensor, VirtualAnalogOutput, HeatingZone, OneWire, LinuxLocalTime, HeatingCircuit, \
    HeatingBoiler, Updater, Modbus, ModbusDevice, ModbusCard, ProgressBar, OutdoorSensor
from app.utils.hardware_type import correct_sheme, get_hardware_type
from app.utils.modbuss import modbuss_to_ini, save_modbus_device, save_modbus_cards
from app.utils.onewire import get_objects, onewire_to_ini
from app.utils.sheme import sheme_to_db
from app.utils.zones_water import write_zones_to_ini


class HomeView(ListView):
    template_name = 'home.html'
    context_object_name = 'digital_outputs'
    queryset = DigitalOutput.objects.all()

    def get_context_data(self, **kwargs):

        context = super(HomeView, self).get_context_data(**kwargs)

        type = get_hardware_type()
        # correct_sheme function accept (DO, AI, AO, DI)
        if type == "lx_1_2":
            context['hardware_type'] = "lx_1_2"
            correct_sheme(10, 4, 4, 2)
        elif type == "lx_1_2l":
            context['hardware_type'] = "lx_1_2l"
            correct_sheme(4, 4, 0, 0)
        elif type == "lx_1":
            context['hardware_type'] = "lx_1"
            correct_sheme(12, 4, 4, 2)
        elif type == "rasp":
            context['hardware_type'] = "rasp"
            correct_sheme(0, 0, 0, 0)
        else:
            context['hardware_type'] = "desktop"
            correct_sheme(0, 0, 0, 0)

        context["analog_inputs"] = AnalogInput.objects.all()
        context["analog_outputs"] = AnalogOutput.objects.all()
        context["digital_inputs"] = DigitalInput.objects.all()
        context["virtual_digital_outputs"] = VirtualDigitalOutput.objects.all()
        context["virtual_sensors"] = VirtualSensor.objects.all()
        context["virtual_analog_outputs"] = VirtualAnalogOutput.objects.all()
        return context


@method_decorator(csrf_exempt, name='dispatch')
class HeatingZoneView(ListView):
    template_name = 'heating_zones.html'
    model = HeatingZone
    context_object_name = "zones"
    paginate_by = 1

    def get_context_data(self, **kwargs):
        context = super(HeatingZoneView, self).get_context_data(**kwargs)
        context['display_zones'] = HeatingZone.objects.all()
        AI = AnalogInput.objects.filter(type="temperature")
        OWFS = OneWire.objects.all()
        VS = VirtualSensor.objects.all()
        DO_fixed = DigitalOutput.objects.all()
        VDO = VirtualDigitalOutput.objects.all()
        MDO = Modbus.objects.filter(type="do")
        MTS = Modbus.objects.filter(type="ts")
        context['tsensors'] = list(chain(AI, OWFS, VS, MTS))
        context['DO'] = list(chain(DO_fixed, VDO, MDO))
        AO_fixed = AnalogOutput.objects.all()
        VAO = VirtualAnalogOutput.objects.all()
        MAO = Modbus.objects.filter(type="ao")
        context['AO'] = list(chain(AO_fixed, VAO, MAO))
        context['selected_ids'] = selected_dos()
        return context

    def post(self, request, *args, **kwargs):
        if request.is_ajax():

            command = request.POST.get('command', '')
            id = request.POST.get('id', '')
            zones_in_json = request.POST.get('heating_zones', '')

            if zones_in_json:
                py_dict = json.loads(zones_in_json)
                write_zones_to_ini(py_dict, parameter="zones")

            if command == "add":
                object = HeatingZone(name="Heating Zone")
                object.save()

            elif command == "delete":
                HeatingZone.objects.get(id=int(id)).delete()

            return JsonResponse({'message': "Success"})


@method_decorator(csrf_exempt, name='dispatch')
class DomesticWaterView(ListView):
    template_name = 'domestic_water.html'
    model = DomesticWater
    context_object_name = "waters"
    paginate_by = 1

    def get_context_data(self, **kwargs):
        context = super(DomesticWaterView, self).get_context_data(**kwargs)
        context['display_waters'] = DomesticWater.objects.all()
        AI = AnalogInput.objects.filter(type="temperature")
        OWFS = OneWire.objects.all()
        VS = VirtualSensor.objects.all()
        DO_fixed = DigitalOutput.objects.all()
        VDO = VirtualDigitalOutput.objects.all()
        MDO = Modbus.objects.filter(type="do")
        MTS = Modbus.objects.filter(type="ts")
        context['tsensors'] = list(chain(AI, OWFS, VS, MTS))
        context['DO'] = list(chain(DO_fixed, VDO, MDO))
        context['selected_ids'] = selected_dos()
        return context

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            command = request.POST.get('command', '')
            id = request.POST.get('id', '')
            water_in_json = request.POST.get('domestic_water', '')

            if water_in_json:
                py_dict = json.loads(water_in_json)
                write_zones_to_ini(py_dict, parameter="waters")

            if command == "add":
                object = DomesticWater(name="Domestic Water")
                object.save()
            elif command == "delete":
                DomesticWater.objects.get(id=int(id)).delete()

            return JsonResponse({'message': "Success"})


class ModbusServerView(ListView):
    template_name = 'modbuss.html'
    model = ModbusServer
    context_object_name = "modbus_server"

    def get_queryset(self):
        return ModbusServer.objects.all()

    def post(self, request, *args, **kwargs):
        enable = request.POST.get('enable_box', '')
        read_only = request.POST.get('read_only_box', '')
        port = request.POST.get('port', '')
        modbuss_to_ini(enable, read_only, port)

        return super(ModbusServerView, self).get(request, *args, **kwargs)


@method_decorator(csrf_exempt, name='dispatch')
class OutdoorSensorView(ListView):
    template_name = 'outdoor_sensor.html'
    model = OutdoorSensor
    context_object_name = "outdoor_sensor"

    def get_queryset(self):
        return OutdoorSensor.objects.all()

    def post(self, request, *args, **kwargs):
        sensor_id = int(request.POST.get('sensor_id', ''))
        table_name = request.POST.get('table_name', '')
        enable_remote = request.POST.get('enable_remote', '')
        data_expiration = request.POST.get('data_expiration', '')
        preferred_data = request.POST.get('preferred_data', '')

        if enable_remote == "true":
            enable_remote = True
        else:
            enable_remote = False

        if data_expiration == "":
            data_expiration = -1

        if enable_remote is False:
            preferred_data = "local"

        if sensor_id == -1 and enable_remote is True:
            preferred_data = "remote"

        OutdoorSensor.objects.all().delete()
        object = OutdoorSensor(sensor_id=sensor_id, table_name=table_name, enable_remote=enable_remote,
                      data_expire=data_expiration, preffered_data=preferred_data)
        object.save()

        return super(OutdoorSensorView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(OutdoorSensorView, self).get_context_data(**kwargs)
        AI = AnalogInput.objects.all()
        AO = AnalogOutput.objects.all()
        VAO = VirtualAnalogOutput.objects.all()
        DI = DigitalInput.objects.all()
        DO = DigitalOutput.objects.all()
        VDO = VirtualDigitalOutput.objects.all()
        OWFS = OneWire.objects.all()
        VS = VirtualSensor.objects.all()
        MBD = ModbusDevice.objects.all()
        context['all_sensors'] = list(chain(AI, AO, VAO, DI, DO, VDO, OWFS, VS, MBD))
        return context


@method_decorator(csrf_exempt, name='dispatch')
class ModbusDeviceView(ListView):
    template_name = 'modbus_devices.html'
    model = ModbusDevice
    context_object_name = "devices"
    paginate_by = 1

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            object = request.POST.get('object', '')
            command = request.POST.get('command', '')
            id = request.POST.get('id', '')
            device_data = json.loads(request.POST.get('device_data', ''))
            cards = json.loads(request.POST.get('cards', ''))

            if device_data:
                save_modbus_device(device_data)
            if cards:
                save_modbus_cards(cards)

            if command == "add" and object == "device":
                object = ModbusDevice(name="Modbus Device")
                object.save()
            elif command == "add" and object == "card":
                device = ModbusDevice.objects.get(id=int(id))
                object = ModbusCard(name="Modbus Card", device=device)
                object.save()
            elif command == "delete" and object == "device":
                ModbusDevice.objects.get(id=int(id)).delete()
            elif command == "delete" and object == "card":
                ModbusCard.objects.get(id=int(id)).delete()

            return JsonResponse({'message': "Success"})

    def get_context_data(self, **kwargs):
        context = super(ModbusDeviceView, self).get_context_data(**kwargs)
        context['display_devices'] = ModbusDevice.objects.all()
        return context


@csrf_exempt
def onewire_ajax(request):
    if request.method == 'GET' and request.is_ajax():
        parameters = request.GET.get('q', '')
        parameters_list = parameters.split("&")

        if 'onewire' in parameters_list:
            objects = get_objects(0, 1)
        else:
            pass
        return JsonResponse({'message': objects})

    if request.method == 'POST' and request.is_ajax():
        parameters = request.GET.get('w', '')
        json_data = request.POST.get('json', '')

        if 'write_onewire_ini' in parameters and json_data:
            py_dict = json.loads(json_data)
            onewire_to_ini(py_dict)
            objects = get_objects(0, 1)

        return JsonResponse({'message': objects})


@csrf_exempt
def sheme_ajax(request):
    if request.method == 'POST' and request.is_ajax():
        digital_outputs = json.loads(request.POST.get('digital_outputs', ''))
        analog_inputs = json.loads(request.POST.get('analog_inputs', ''))
        analog_outputs = json.loads(request.POST.get('analog_outputs', ''))
        digital_inputs = json.loads(request.POST.get('digital_inputs', ''))
        vdo = json.loads(request.POST.get('vdo', ''))
        virtual_sensors = json.loads(request.POST.get('virtual_sensors', ''))
        vao = json.loads(request.POST.get('vao', ''))

        sheme_to_db(digital_outputs, analog_inputs, analog_outputs, digital_inputs, vdo, virtual_sensors, vao)

        return JsonResponse({'message': "USPESNO POSLANO!"})


@method_decorator(csrf_exempt, name='dispatch')
class MainView(TemplateView):
    template_name = 'main.html'

    def post(self, request, *args, **kwargs):
        update = request.POST.get('update', '')
        city = request.POST.get('city', '')

        if city:
            i = LinuxLocalTime.objects.all().count()
            if i == 0:
                object = LinuxLocalTime(city=city)
                object.save()
            else:
                object = LinuxLocalTime.objects.all().first()
                object.city = city
                object.save()
            ut = LinuxTools()
            ut.setLocaltimeEurope(city)
            if request.is_ajax():
                return JsonResponse({'message': "City set!"})
            else:
                return HttpResponseRedirect(reverse_lazy('app:main'))

        if update == "gui":
            try:
                gui_app = get_app_gui_version()
                latest_gui = float(gui_app[0])
            except:
                latest_gui = float(GUI_VERSION) + 1

            if Updater.objects.all().count() == 0:
                object = Updater(current_gui=float(GUI_VERSION), latest_gui=latest_gui, update_gui=True)
                object.save()
            else:
                object = Updater.objects.all().first()
                object.current_gui = GUI_VERSION
                object.latest_gui = latest_gui
                object.update_gui = True
                object.save()

            root_parant = os.path.dirname(BASE_DIR)
            updater_path = os.path.join(BASE_DIR, 'updater.py')
            shutil.copy2(updater_path, root_parant)
            os.chdir(root_parant)
            exe_file = root_parant + "/updater.py"
            log_file = root_parant + "/update_log.txt"
            os.system("chmod 755 " + exe_file)
            os.system(exe_file + " > " + log_file + " 2>&1 &")

        if update == "app":

            base_dir = os.path.dirname(BASE_DIR)
            app_path = os.path.join(base_dir, 'app_tmp')
            os.chdir(app_path)
            file_paths = []
            for subdir, dirs, files in os.walk(app_path):
                for file in files:
                    filepath = subdir + os.sep + file
                    file_paths.append(filepath)

            if file_paths:
                try:
                    pc = Pipcontroller()
                    pc.updatePipc(file_paths)
                    gui_app = get_app_gui_version()
                    current_app = float(gui_app[1])
                    latest_app = float(gui_app[1])
                except:
                    current_app = Updater.objects.all().first().current_app
                    latest_app = Updater.objects.all().first().latest_app

                if Updater.objects.all().count() == 0:
                    object = Updater(current_app=current_app, latest_app=latest_app, update_app=True)
                    object.save()
                else:
                    object = Updater.objects.all().first()
                    object.current_app = current_app
                    object.latest_app = latest_app
                    object.update_app = True
                    object.save()

        return HttpResponseRedirect(reverse_lazy('app:main'))

    def get_context_data(self, **kwargs):
        context = super(MainView, self).get_context_data(**kwargs)
        if Updater.objects.all().count() == 0:
            object = Updater(current_gui=float(GUI_VERSION), current_app=0.0)
            object.save()

        context['hardware_type'] = get_hardware_type()
        context['gui_message'] = ""
        context['app_message'] = ""
        try:
            gui_app = get_app_gui_version()
            context['current_gui'] = float(GUI_VERSION)
            context['latest_gui'] = float(gui_app[0])
            context['current_app'] = Updater.objects.all().first().current_app
            context['latest_app'] = float(gui_app[1])
        except:
            context['current_gui'] = float(GUI_VERSION)
            context['latest_gui'] = float(GUI_VERSION)
            context['current_app'] = Updater.objects.all().first().current_app
            context['latest_app'] = Updater.objects.all().first().latest_app

            context['gui_message'] = "Informations about the versions are currently unavailable."
            context['app_message'] = "Informations about the versions are currently unavailable."
            context['flag'] = True

        if Updater.objects.all().count() != 0:
            object = Updater.objects.all().first()
            l_gui = object.latest_gui
            c_app = object.current_app
            if object.update_gui is True and l_gui == float(GUI_VERSION):
                object.update_gui = False
                object.save()
                context['gui_message'] = "Gui was successfully updated"
            elif object.update_gui is True and l_gui != float(GUI_VERSION):
                object.update_gui = False
                object.save()
                context['gui_message'] = "Error: Gui wasn't updated successfully!"
            elif object.update_app is True and c_app == context['latest_app']:
                object.update_app = False
                object.save()
                context['app_message'] = "Application was successfully updated"
            elif object.update_app is True and c_app != context['latest_app']:
                object.update_app = False
                object.save()
                context['app_message'] = "Error: Application wasn't updated successfully"

        if ProgressBar.objects.all().count() == 0:
            bar = ProgressBar(percentage=0)
            bar.save()
        else:
            bar = ProgressBar.objects.all().first()
            bar.percentage = 0
            bar.save()

        ut = LinuxTools()
        current_city = ut.localtime()
        if current_city:
            context['city_selected'] = current_city.strip()
        if not current_city:
            context['city_selected'] = LinuxLocalTime.objects.all().first()
        context['cities'] = ut.getCitiesEurope()
        pc = Pipcontroller()
        context['serial'] = pc.get_serial()
        context['realtime'] = subprocess.check_output(["date", "+%D %H:%M"]).decode('ascii')
        return context


class StatusView(TemplateView):
    template_name = 'status.html'

    def post(self, request, *args, **kwargs):
        pc = Pipcontroller()
        if request.is_ajax():
            status = request.POST.get('status_request', '')
            status_xml = request.POST.get('status_xml', '')
            if status == "status":
                controller_status = pc.pipcontroller_running()
                watchdog_status = pc.pipwatchdog_running()
                broadcast_status = pc.broadcastProcRunning()
                return JsonResponse({'controller_status': controller_status, 'watchdog_status': watchdog_status,
                                     'broadcast_status': broadcast_status})
            if status_xml:
                status_xml = pc.status()
                return HttpResponse(status_xml)
            else:
                return JsonResponse({'message': "Failed"})

        controller = request.POST.get('controller', '')
        watchdog = request.POST.get('watchdog', '')
        broadcast = request.POST.get('broadcast', '')
        if controller == "start":
            pc.start()
        elif controller == "stop":
            pc.stop()
        if watchdog == "start":
            pc.start_pipwatchdog()
        elif watchdog == "stop":
            pc.kill_pipwatchdog()
        if broadcast == "start":
            pc.start_broadcast()
        elif broadcast == "stop":
            pc.kill_broadcast()
        return HttpResponseRedirect(reverse_lazy('app:status'))

    def get_context_data(self, **kwargs):
        context = super(StatusView, self).get_context_data(**kwargs)
        pc = Pipcontroller()
        context['controller_status'] = pc.pipcontroller_running()
        context['watchdog_status'] = pc.pipwatchdog_running()
        context['status_xml'] = pc.status()
        context['broadcast_status'] = pc.broadcastProcRunning()
        context['frontpanel_status'] = pc.pipcfrontProcRunning()
        return context


@method_decorator(csrf_exempt, name='dispatch')
class HeatingCircuitView(ListView):
    template_name = "heating_circuit.html"
    model = HeatingCircuit
    context_object_name = "circuits"
    paginate_by = 1

    def get_context_data(self, **kwargs):
        context = super(HeatingCircuitView, self).get_context_data(**kwargs)
        context['display_circuits'] = HeatingCircuit.objects.all()
        AI = AnalogInput.objects.filter(type="temperature")
        OWFS = OneWire.objects.all()
        VS = VirtualSensor.objects.all()
        DO_fixed = DigitalOutput.objects.all()
        VDO = VirtualDigitalOutput.objects.all()
        MDO = Modbus.objects.filter(type="do")
        MTS = Modbus.objects.filter(type="ts")
        context['tsensors'] = list(chain(AI, OWFS, VS, MTS))
        context['DO'] = list(chain(DO_fixed, VDO, MDO))
        context['HZ'] = HeatingZone.objects.all()
        context['DHW'] = DomesticWater.objects.all()
        context['selected_ids'] = selected_dos()
        return context

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            command = request.POST.get('command', '')
            id = request.POST.get('id', '')
            circuits_in_json = request.POST.get('heating_circuit', '')

            if circuits_in_json:
                py_dict = json.loads(circuits_in_json)
                write_zones_to_ini(py_dict, parameter="circuit")

            if command == "add":
                object = HeatingCircuit(name="Heating Circuit")
                object.save()
            elif command == "delete":
                HeatingCircuit.objects.get(id=int(id)).delete()

            return JsonResponse({'message': "Success"})


@method_decorator(csrf_exempt, name='dispatch')
class HeatingBoilerView(ListView):
    template_name = "heating_boiler.html"
    model = HeatingBoiler
    context_object_name = "boilers"
    paginate_by = 1

    def get_context_data(self, **kwargs):
        context = super(HeatingBoilerView, self).get_context_data(**kwargs)
        context['display_boilers'] = HeatingBoiler.objects.all()
        AI = AnalogInput.objects.filter(type="temperature")
        OWFS = OneWire.objects.all()
        VS = VirtualSensor.objects.all()
        DO_fixed = DigitalOutput.objects.all()
        VDO = VirtualDigitalOutput.objects.all()
        MTS = Modbus.objects.filter(type="ts")
        MDO = Modbus.objects.filter(type="do")
        context['tsensors'] = list(chain(AI, OWFS, VS, MTS))
        context['DO'] = list(chain(DO_fixed, VDO, MDO))
        context['HZ'] = HeatingZone.objects.all()
        context['DHW'] = DomesticWater.objects.all()
        context['HC'] = HeatingCircuit.objects.all()
        context['selected_ids'] = selected_dos()
        return context

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            command = request.POST.get('command', '')
            id = request.POST.get('id', '')
            boilers_in_json = request.POST.get('heating_boiler', '')

            if boilers_in_json:
                py_dict = json.loads(boilers_in_json)
                write_zones_to_ini(py_dict, parameter="boiler")

            if command == "add":
                object = HeatingBoiler(name="Heating Boiler")
                object.save()
            elif command == "delete":
                HeatingBoiler.objects.get(id=int(id)).delete()

            return JsonResponse({'message': "Success"})


@csrf_exempt
def progress_bar(request):
    if request.method == 'POST' and request.is_ajax():
        update = request.POST.get('update', '')
        mark = request.POST.get('mark', '')

        if update == "gui" and mark == "first":
            updater = Updater.objects.all().first()
            updater.update_gui = True
            updater.save()

            url = "https://panel.i13tech.com/firmware/get.php?API-KEY=c4MctNk+92T1GtL2ICaTxwZMBteQtE9moK6qcHVC"
            a = {"app_download_request": "pipcwebmod", "get_app_info": False}
            gui_data = json.dumps(a).encode('utf-8')
            gui = Request(url, gui_data, {'Content-Type': 'application/json'})

            root = os.path.dirname(BASE_DIR)
            gui_path = os.path.join(root, "gui_tmp")
            gui_zipath = os.path.join(root, "gui_zip.zip")

            try:
                gui_response = urlopen(gui, timeout=30)
                file_size = gui_response.getheader('Content-Length')

                output = open(gui_zipath, "wb")
                chunk = 128000
                percentage = 0
                while 1:
                    object = ProgressBar.objects.all().first()
                    data = output.write(gui_response.read(chunk))
                    if not data:
                        print("Total bytes downloaded: {}".format(file_size))
                        object.percentage = 100
                        object.save()
                        output.close()
                        break
                    percentage += (data / int(file_size)) * 100
                    object.percentage = int(percentage)
                    object.save()
                    print("Zipping: {}".format(int(percentage)))

                zip = zipfile.ZipFile(gui_zipath)
                zip.extractall(gui_path)
                zip.close()
                os.remove(gui_zipath)
            except:
                object = ProgressBar.objects.all().first()
                object.percentage = 999
                object.save()

                return JsonResponse({'value': 999})
            return HttpResponse('')

        if update == "app" and mark == "first":
            updater = Updater.objects.all().first()
            updater.update_app = True
            updater.save()

            downloadApp(BASE_DIR)

            return HttpResponse('')

        if update == "gui" or update == "app" and mark == "second":
            bar = ProgressBar.objects.all().first()
            progress = bar.percentage
            print("Progress: {}".format(progress))
            return JsonResponse({'value': progress})
