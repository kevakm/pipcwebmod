from django.contrib import admin

from app.models import DomesticWater, ModbusServer, DigitalOutput, AnalogInput, DigitalInput, AnalogOutput, OneWire, \
    VirtualDigitalOutput, VirtualSensor, VirtualAnalogOutput, HeatingZone, LinuxLocalTime, HeatingCircuit, HeatingBoiler, \
    Updater, Modbus, ModbusDevice, ModbusCard, ProgressBar


class DisplayIds(admin.ModelAdmin):
    list_display = ["__str__", "id"]

admin.site.register(OneWire, DisplayIds)
admin.site.register(DomesticWater, DisplayIds)
admin.site.register(ModbusServer, DisplayIds)
admin.site.register(DigitalOutput, DisplayIds)
admin.site.register(DigitalInput, DisplayIds)
admin.site.register(AnalogInput, DisplayIds)
admin.site.register(AnalogOutput, DisplayIds)
admin.site.register(VirtualDigitalOutput, DisplayIds)
admin.site.register(VirtualSensor, DisplayIds)
admin.site.register(VirtualAnalogOutput, DisplayIds)
admin.site.register(HeatingZone, DisplayIds)
admin.site.register(HeatingCircuit, DisplayIds)
admin.site.register(HeatingBoiler, DisplayIds)
admin.site.register(LinuxLocalTime, DisplayIds)
admin.site.register(Updater, DisplayIds)
admin.site.register(ModbusDevice, DisplayIds)
admin.site.register(ModbusCard, DisplayIds)
admin.site.register(Modbus, DisplayIds)
admin.site.register(ProgressBar)
