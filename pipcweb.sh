#!/bin/bash

dir="/pipc/pipcwebmod"
first_run_file="$dir/first_run.inf"
manage="python3 $dir/manage.py"

if true || ! [ -e "$first_run_file" ] || ! [ -e "$dir/db.sqlite3" ]
then
        echo "Performing first run"
        $manage migrate
#       $manage createsuperuser
        echo 1 > $first_run_file
else
        echo "First run not required"
fi


echo "Starting server"
$manage runserver 0.0.0.0:8000 --insecure


