"use strict";

var OWSensors = {

    sensors: [],
    
    writeSensorsIniFiles: function() {
        
        var Self = OWSensors;
        var output = [];                

        for (var j in Self.sensors) {

            var s = Self.sensors[j];
            if (s.registered === true) {
                output.push({
                    address_ini: s.address_ini,
                    bus_id: s.bus_id,
                    trim: s.trim,
                    name: s.name
                });
            }
        }
        
        console.log("writing ow ini files");
        
        ajaxPost("w=write_onewire_ini", output, 
            { onSuccess: Self.onSensorUpdate, onError: Self.onSensorUpdate});    
    },
    
    addSensor: function(address_ini, trim, name) {
        
        var Self = OWSensors;
        
        for (var j in Self.sensors) {
            if (Self.sensors[j].address_ini == address_ini) {
                Self.sensors[j].registered = true;
                Self.sensors[j].trim = trim;
                Self.sensors[j].name = name;
            }
        }

        Self.writeSensorsIniFiles();
    },
    
    onSensorUpdate: function(data) {
        
        console.log("onSensorUpdate");
        console.log(data);
        if (!data) {
            console.error("No data");
            return;
        }
        var Self = OWSensors;
                
        var sensors_owfs = data.owfs_sensors;
        var sensors_ini = data.onewire_ini;

        var $wrapper = $("#ow-sensors");
        $wrapper.html('');


        // Merge sensors
        Self.sensors = [];

        for (var i in sensors_ini) {

            var si = sensors_ini[i];
            Self.sensors.push({
                address: si['address'].toUpperCase(),
                address_ini: si['address_ini'].toUpperCase(),
                bus_id: si['bus_id'],
                registered: true, // is in ini file
                trim: si.trim,
                name: si.name
            });
        }

        for (var i in sensors_owfs) {

            var si = sensors_owfs[i];
            var registered = false;
            for (var j in Self.sensors) {
                if (Self.sensors[j].address_ini == si.address_ini.toUpperCase()) {
                    registered = true;
                    console.log("Sensor registered");
                    break;
                }
            }

            if (!registered) {
                console.log("Not registered !!!!!!! " + si.address_ini);
                Self.sensors.push({
                    address: si['address'].toUpperCase(),
                    address_ini: si['address_ini'].toUpperCase(),
                    bus_id: si['bus_id'],
                    registered: false, // is in ini file
                    trim: 0,
                    name: ""
                });
            }

        }


        var $table = $("<table>");
        var $thead = $("<thead> <tr> <th>Address</th> <th>Bus</th> <th>Trim</th> <th>Name</th> <th></th> </tr> </thead>").appendTo($table);
        var $tbody = $("<tbody>").appendTo($table);

        for (var i in Self.sensors) {

            var si = Self.sensors[i];
            var $row = $("<tr>")
                    .attr("address_ini", si['address_ini'])
                    .appendTo($tbody);
            var html =
                    "<td>" + si['address'].toUpperCase() + "</td>"
                    + '<td class="sensor-bus_id">' + si['bus_id'] + '</td>'
                    + '<td><input class="ow-trim form-control" type="number" value="' + si.trim + '" step="0.1" sensor_addr="' + si['address'] + '"></td>'
                    + '<td><input class="ow-name form-control" type="text" sensor_addr="' + si['address'] + '" value="' + si['name'] + '"></td>'
                    ;
            if (!si.registered) {
                html += '<td><button class="btn btn-default add-sensor-btn">Add</button></td>';
            } else {
                html += '<td><button class="btn btn-default add-sensor-btn">Update</button></td>'
            }
            $row.html(html);

            $row.find(".add-sensor-btn").click(function () {
                var $r = $(this).parent().parent();
                var bus = parseInt($r.find(".sensor-bus_id").html());
                var address_ini = $r.attr('address_ini');
                var trim = $r.find(".ow-trim").val();
                var name = $r.find(".ow-name").val();
                console.log("Add clicked " + address_ini + " bus:" + bus
                        + " trim=" + trim
                        + " name=" + name);
                Self.addSensor(address_ini, trim, name);
            });

        }
        

        $table.appendTo($wrapper);
    },
    
};


