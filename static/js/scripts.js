"use strict";

$(document).ready(function () {
    ajaxGet("onewire&neki&druga", null, {onSuccess: OWSensors.onSensorUpdate});
});

$(document).on("click", "#get_button", function () {
    ajaxGet("onewire&neki&druga", null, {onSuccess: OWSensors.onSensorUpdate});
});

function ajaxGet(url_params, snddata, options) {

    var url = static_url + '?q=' + url_params;
    var csrftoken = getCookie('csrftoken')

    var cb_success = null;
    var cb_error = null;
    var event_type = null;
    if (options) {
        if (options.onSuccess) {
            cb_success = options.onSuccess;
        }
        if (options.onError) {
            cb_error = options.onError;
        }
        if (options.eventType) {
            event_type = options.eventType;
        }
    }

    console.log(url);

    $.ajax({
        method: "GET",
        dataType: "json",
        url: url,
        data: {csrfmiddlewaretoken: csrftoken},
        beforeSend: function (data) {
            //pokaži loading mogoče
        },
        complete: function (data) {
            //debugMsg(data);
        },
        success: function (data) {

            var data = JSON.parse(data.message)
            console.log("ON success GET!")
            console.log(data);

            // callback
            if (cb_success && cb_success.constructor === Function) {

                cb_success(data);

            } else if (cb_success && cb_success.constructor === Array) {

                for (var i = 0; i < cb_success.length; i++) {

                    if (cb_success[i] && cb_success[i].constructor === Function) {
                        cb_success[i](data);
                    }
                }
            }

            return data;
        },
        error: function (data) {

            console.error("error ajax ");
            console.error(data);

            if (cb_error && cb_error.constructor === Function) {

                cb_error(data);

            } else if (cb_error && cb_error.constructor === Array) {

                for (var i = 0; i < cb_error.length; i++) {

                    if (cb_error[i] && cb_error[i].constructor === Function) {
                        cb_error[i](data);
                    }
                }
            }
        }
    });
}


// @options:
//      onSuccess
//      onError
function ajaxPost(url_params, data, options) {

    var url = static_url + '?' + url_params;
    var csrftoken = getCookie('csrftoken')
    var json = JSON.stringify(data);

    var cb_success = null;
    if (options && options.onSuccess) {
        cb_success = options.onSuccess;
    }
    var cb_error = null;
    if (options && options.onError) {
        cb_error = options.onError;
    }

//    debugMsg(url);
//    debugMsg(json);

    $.ajax({
        type: "POST",
        dataType: "json",
        url: url,
        data: {
            csrfmiddlewaretoken: csrftoken,
            json: json,
        },
        beforeSend: function (data) {
            //pokaži loading mogoče
        },
        complete: function (data) {
            //debugMsg(data);                
        },
        success: function (data) {

            var data = JSON.parse(data.message);
            console.log("ON SUCCESS POST:")
            console.log(data);
            
            // callback                     
            if (cb_success && cb_success.constructor === Function) {

                cb_success(data);

            } else if (cb_success && cb_success.constructor === Array) {

                for (var i = 0; i < cb_success.length; i++) {

                    if (cb_success[i] && cb_success[i].constructor === Function) {
                        cb_success[i](data);
                    }
                }
            }
            return data;
        },
        error: function (data) {

            console.error(data);

            if (data.responseJSON && data.responseJSON.info == "Session not valid") {
                //window.location.href = "login.php"
            }

            if (cb_error && cb_error.constructor === Function) {

                cb_error(data);

            } else if (cb_error && cb_error.constructor === Array) {

                for (var i = 0; i < cb_error.length; i++) {

                    if (cb_error[i] && cb_error[i].constructor === Function) {
                        cb_error[i](data);
                    }
                }
            }

        }
    });
}

    
