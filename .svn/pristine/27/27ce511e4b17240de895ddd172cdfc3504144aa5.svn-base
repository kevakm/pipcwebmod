import json
import os

from pipcwebmod.settings import PIPC_DIR
from app.models import DomesticWater, HeatingZone, HeatingCircuit, HeatingBoiler
from app.utils.read_all_files import validate_first_char


def read_heating_zones():
    path = os.path.join(PIPC_DIR, 'ctrl_sett', 'hccontrol.ini')

    if os.path.exists(path) == False:
        with open(path, "w") as file:
            file.write("")
        return False
    else:
        with open(path, 'r') as file:
            content = []
            for line in file:
                if validate_first_char(line) == True:
                    content.append(line)
            content = [x.strip() for x in content]
            seznam = []
            for row in content:
                if "name" in row:
                    seznam.append(row)
                elif "id" in row:
                    seznam.append(row)
                elif "heating" in row:
                    seznam.append(row)
                elif "cooling" in row:
                    seznam.append(row)
                elif "opt" in row:
                    seznam.append(row)
            chunks = [seznam[i:i + 5] for i in range(0, len(seznam), 5)]
        return chunks


def write_zones_to_ini(py_dict, parameter):

    if parameter == "zones":

        for zone in py_dict:

            id = int(zone['id'])
            name = zone['name']

            if zone['ts_id'] is None:
                ts_id = -1
            else:
                ts_id = zone['ts_id']

            if zone['fixed_temp'] is None:
                fixed_temp = 0
            else:
                fixed_temp = zone['fixed_temp']

            if zone['heating_type'] is None:
                heating_type = 'undefined'
            else:
                heating_type = zone['heating_type']

            if zone['control_type'] is None:
                control_type = 'undefined'
            else:
                control_type = zone['control_type']

            if zone['ctrl_type'] is None:
                ctrl_type = 'undefined'
            else:
                ctrl_type = zone['ctrl_type']

            parameters = json.dumps(zone['parameters'])

            object = HeatingZone.objects.filter(id=id)
            if object.exists():
                object = HeatingZone.objects.get(id=id)
                object.name = name
                object.heating = zone['heating']
                object.cooling = zone['cooling']
                object.optimization = zone['optimization']
                object.ts_id = ts_id
                object.ts_table = zone['ts_table']
                object.fixed_temp = fixed_temp
                object.heating_type = heating_type
                object.control_type = control_type
                object.ctrl_type = ctrl_type
                object.parameters = parameters
                object.save()

    elif parameter == "waters":
        for water in py_dict:
            id = int(water['id'])
            name = water['name']

            if water['optimization'] is False:
                opt = 0
            else:
                opt = 1

            if water['ts_id'] is None:
                ts_id = -1
            else:
                ts_id = water['ts_id']

            if water['fixed_temp'] is None:
                fixed_temp = 0
            else:
                fixed_temp = water['fixed_temp']

            if water['control_type'] is None:
                control_type = 'undefined'
            else:
                control_type = water['control_type']

            parameters = json.dumps(water['parameters'])

            object = DomesticWater.objects.filter(id=id)
            if object.exists():
                object = DomesticWater.objects.get(id=id)
                object.name = name
                object.optimization = opt
                object.ts_id = ts_id
                object.ts_table = water['ts_table']
                object.fixed_temp = fixed_temp
                object.control_type = control_type
                object.parameters = parameters
                object.save()

    elif parameter == "circuit":
        for circuit in py_dict:

            id = int(circuit['id'])
            name = circuit['name']

            if circuit['ts_id'] is None:
                ts_id = -1
            else:
                ts_id = circuit['ts_id']

            if circuit['circuit_type'] is None:
                circuit_type = 'undefined'
            else:
                circuit_type = circuit['circuit_type']

            if circuit['ctrl_type'] is None:
                ctrl_type = 'undefined'
            else:
                ctrl_type = circuit['ctrl_type']

            parameters = json.dumps(circuit['parameters'])

            object = HeatingCircuit.objects.filter(id=id)
            if object.exists():
                object = HeatingCircuit.objects.get(id=id)
                object.name = name
                object.ts_id = ts_id
                object.ts_table = circuit['ts_table']
                object.circuit_type = circuit_type
                object.ctrl_type = ctrl_type
                object.hz.clear()
                object.dhw.clear()
                if circuit['hz']:
                    for zone in circuit['hz']:
                        z = HeatingZone.objects.get(id=int(zone))
                        object.hz.add(z)
                if circuit['dhw']:
                    for water in circuit['dhw']:
                        w = DomesticWater.objects.get(id=int(water))
                        object.dhw.add(w)
                object.parameters = parameters
                object.save()

    elif parameter == "boiler":

        for boiler in py_dict:

            id = int(boiler['id'])
            name = boiler['name']

            if boiler['ts_id'] is None:
                ts_id = -1
            else:
                ts_id = boiler['ts_id']

            if boiler['boiler_type'] is None:
                boiler_type = 'undefined'
            else:
                boiler_type = boiler['boiler_type']

            if boiler['ctrl_type'] is None:
                ctrl_type = 'undefined'
            else:
                ctrl_type = boiler['ctrl_type']

            parameters = json.dumps(boiler['parameters'])

            object = HeatingBoiler.objects.filter(id=id)
            if object.exists():
                object = HeatingBoiler.objects.get(id=id)
                object.name = name
                object.ts_id = ts_id
                object.ts_table = boiler['ts_table']
                object.boiler_type = boiler_type
                object.ctrl_type = ctrl_type
                object.hz.clear()
                object.dhw.clear()
                object.hc.clear()
                if boiler['hz']:
                    for zone in boiler['hz']:
                        z = HeatingZone.objects.get(id=int(zone))
                        object.hz.add(z)
                if boiler['dhw']:
                    for water in boiler['dhw']:
                        w = DomesticWater.objects.get(id=int(water))
                        object.dhw.add(w)
                if boiler['hc']:
                    for circuit in boiler['hc']:
                        c = HeatingCircuit.objects.get(id=int(circuit))
                        object.hc.add(c)
                object.parameters = parameters
                object.save()


def chunk_scan(chunk, key):
    for element in chunk:
        k = element.split("=", 1)[0]
        v = element.split("=", 1)[1]

        if k == key:
            return v

    return False


def get_heating_zones_contex(chunks):
    context = ""
    for chunk in chunks:
        id = -1
        name = ""
        heating = 0
        cooling = 0
        opt = 0

        tmp = chunk_scan(chunk, "id")
        if tmp != False:
            id = tmp

        tmp = chunk_scan(chunk, "name_ini")
        if tmp != False:
            name = tmp

        tmp = chunk_scan(chunk, "heating")
        if tmp != False:
            heating = int(tmp)

        tmp = chunk_scan(chunk, "cooling")
        if tmp != False:
            cooling = int(tmp)

        tmp = chunk_scan(chunk, "opt")
        if tmp != False:
            opt = int(tmp)

        context += """<tr><td><input type="text" class="form-control" name="heating_zone" value={}></td>""".format(
            name)
        if heating == 0:
            context += """<td><input type="checkbox" name="heating_box"></td>"""
        else:
            context += """<td><input type="checkbox" name="heating_box" checked="checked"></td>"""
        if cooling == 0:
            context += """<td><input type="checkbox" name="cooling_box"></td>"""
        else:
            context += """<td><input type="checkbox" name="cooling_box" checked="checked"></td>"""
        if opt == 0:
            context += """<td><input type="checkbox" name="optimization_box"></td>"""
        else:
            context += """<td><input type="checkbox" name="optimization_box" checked="checked"></td>"""
        context += """<td><button type="submit" class="btn btn-default btn-delete-zone">Delete</button></td></tr>"""

    return context
