import json
import os
import shutil
from datetime import datetime
from itertools import chain

import time

import subprocess

from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import DetailView
from django.views.generic import ListView
from django.views.generic import TemplateView

from app.utils.selected_dos import selected_dos
from pipctools import LinuxTools, Pipcontroller
from pipcwebmod import settings
from pipcwebmod.settings import BASE_DIR, PIPC_DIR
from app.models import DomesticWater, Modbuss, DigitalOutput, AnalogInput, AnalogOutput, DigitalInput, \
    VirtualDigitalOutput, VirtualSensor, VirtualAnalogOutput, HeatingZone, OneWire, LinuxLocalTime, HeatingCircuit, \
    HeatingBoiler
from app.utils.hardware_type import correct_sheme
from app.utils.iostrings import update_iostrings
from app.utils.modbuss import modbuss_to_ini
from app.utils.onewire import get_objects, onewire_to_ini
from app.utils.read_all_files import from_files_to_db
from app.utils.sheme import sheme_to_db
from app.utils.wipe_db_files import delete_files, delete_db
from app.utils.zones_water import read_heating_zones, get_heating_zones_contex, write_zones_to_ini


class HomeView(ListView):

    template_name = 'home.html'
    context_object_name = 'digital_outputs'
    queryset = DigitalOutput.objects.all()

    def get_context_data(self, **kwargs):

        # from_files_to_db()

        context = super(HomeView, self).get_context_data(**kwargs)
        path = os.path.join(PIPC_DIR, 'dev', 'hwd_info.txt')
        with open(path, "r") as file:
            first_line = file.readline()
            type = first_line.split("=", 1)[1]
            type = type.strip()

            # correct_sheme function accept (DO, AI, AO, DI)
            if type == "Lx_1_2" or type == "lx_1_2":
                context['hardware_type'] = "lx_1_2"
                correct_sheme(10, 4, 4, 2)
            elif type == "Lx_1_2l" or type == "lx_1_2l":
                context['hardware_type'] = "lx_1_2l"
                correct_sheme(4, 4, 0, 0)
            elif type == "Lx_1" or type == "lx_1":
                context['hardware_type'] = "lx_1"
                correct_sheme(12, 4, 4, 2)
            elif type == "Rasp" or type == "rasp":
                context['hardware_type'] = "rasp"
                correct_sheme(0, 0, 0, 0)
            else:
                context['hardware_type'] = "desktop"
                correct_sheme(0, 0, 0, 0)

        context["analog_inputs"] = AnalogInput.objects.all()
        context["analog_outputs"] = AnalogOutput.objects.all()
        context["digital_inputs"] = DigitalInput.objects.all()
        context["virtual_digital_outputs"] = VirtualDigitalOutput.objects.all()
        context["virtual_sensors"] = VirtualSensor.objects.all()
        context["virtual_analog_outputs"] = VirtualAnalogOutput.objects.all()
        return context


@method_decorator(csrf_exempt, name='dispatch')
class HeatingZoneView(ListView):
    template_name = 'heating_zones.html'
    model = HeatingZone
    context_object_name = "zones"
    paginate_by = 1

    def get_context_data(self, **kwargs):
        context = super(HeatingZoneView, self).get_context_data(**kwargs)
        context['display_zones'] = HeatingZone.objects.all()
        AI = AnalogInput.objects.filter(type=0)
        OWFS = OneWire.objects.all()
        VS = VirtualSensor.objects.all()
        DO_fixed = DigitalOutput.objects.all()
        VDO = VirtualDigitalOutput.objects.all()
        context['tsensors'] = list(chain(AI, OWFS, VS))
        context['DO'] = list(chain(DO_fixed, VDO))
        AO_fixed = AnalogOutput.objects.all()
        VAO = VirtualAnalogOutput.objects.all()
        context['AO'] = list(chain(AO_fixed, VAO))
        context['selected_ids'] = selected_dos()
        return context

    def post(self, request, *args, **kwargs):
        if request.is_ajax():

            command = request.POST.get('command', '')
            id = request.POST.get('id', '')
            zones_in_json = request.POST.get('heating_zones', '')

            if zones_in_json:
                py_dict = json.loads(zones_in_json)
                write_zones_to_ini(py_dict, parameter="zones")

            if command == "add":
                object = HeatingZone(name="HZ")
                object.save()
            elif command == "delete":
                HeatingZone.objects.get(id=int(id)).delete()

            # delete_id = request.POST.get('delete_id', '')
            # add_id = request.POST.get('add_id', '')
            # if add_id:
            #     object = HeatingZone(name="HZ", parameters={})
            #     object.save()
            # if delete_id:
            #     HeatingZone.objects.get(id=int(delete_id)).delete()
            # if zones_in_json:
            #     py_dict = json.loads(zones_in_json)
            #     path = os.path.join(PIPC_DIR, 'ctrl_sett', 'hccontrol.ini')
            #     write_zones_to_ini(path, py_dict, parameter="zones")

            return JsonResponse({'message': "Success"})


@method_decorator(csrf_exempt, name='dispatch')
class DomesticWaterView(ListView):
    template_name = 'domestic_water.html'
    model = DomesticWater
    context_object_name = "waters"
    paginate_by = 1

    def get_context_data(self, **kwargs):
        context = super(DomesticWaterView, self).get_context_data(**kwargs)
        context['display_waters'] = DomesticWater.objects.all()
        AI = AnalogInput.objects.filter(type=0)
        OWFS = OneWire.objects.all()
        VS = VirtualSensor.objects.all()
        DO_fixed = DigitalOutput.objects.all()
        VDO = VirtualDigitalOutput.objects.all()
        context['tsensors'] = list(chain(AI, OWFS, VS))
        context['DO'] = list(chain(DO_fixed, VDO))
        context['selected_ids'] = selected_dos()
        return context

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            command = request.POST.get('command', '')
            id = request.POST.get('id', '')
            water_in_json = request.POST.get('domestic_water', '')

            if water_in_json:
                py_dict = json.loads(water_in_json)
                write_zones_to_ini(py_dict, parameter="waters")

            if command == "add":
                object = DomesticWater(name="DHW")
                object.save()
            elif command == "delete":
                DomesticWater.objects.get(id=int(id)).delete()

            return JsonResponse({'message': "Success"})


class ModbussView(ListView):
    template_name = 'modbuss.html'
    model = Modbuss

    def get_queryset(self):
        return Modbuss.objects.all()

    def post(self, request, *args, **kwargs):
        enable = request.POST.get('enable_box', '')
        read_only = request.POST.get('read_only_box', '')
        port = request.POST.get('port', '')
        modbuss_to_ini(enable, read_only, port)

        return super(ModbussView, self).get(request, *args, **kwargs)


@csrf_exempt
def onewire_ajax(request):

    if request.method == 'GET' and request.is_ajax():
        parameters = request.GET.get('q', '')
        parameters_list = parameters.split("&")

        if 'onewire' in parameters_list:
            objects = get_objects(0, 1)
        else:
            pass
        return JsonResponse({'message': objects})

    if request.method == 'POST' and request.is_ajax():
        parameters = request.GET.get('w', '')
        json_data = request.POST.get('json', '')

        if 'write_onewire_ini' in parameters and json_data:
            py_dict = json.loads(json_data)
            onewire_to_ini(py_dict)
            objects = get_objects(0, 1)
            # update_iostrings()

        return JsonResponse({'message': objects})


@csrf_exempt
def sheme_ajax(request):

    if request.method == 'POST' and request.is_ajax():
        digital_outputs = json.loads(request.POST.get('digital_outputs', ''))
        analog_inputs = json.loads(request.POST.get('analog_inputs', ''))
        analog_outputs = json.loads(request.POST.get('analog_outputs', ''))
        digital_inputs = json.loads(request.POST.get('digital_inputs', ''))
        vdo = json.loads(request.POST.get('vdo', ''))
        virtual_sensors = json.loads(request.POST.get('virtual_sensors', ''))
        vao = json.loads(request.POST.get('vao', ''))

        sheme_to_db(digital_outputs, analog_inputs, analog_outputs, digital_inputs, vdo, virtual_sensors, vao)
        # update_iostrings()

        return JsonResponse({'message': "USPESNO POSLANO!"})


def wipe_files(request):
    delete_files()
    return HttpResponseRedirect('/')


def wipe_db(request):
    delete_db()
    return HttpResponseRedirect('/')


class MainView(TemplateView):
    template_name = 'main.html'

    def post(self, request, *args, **kwargs):
        update = request.POST.get('update', '')
        city = request.POST.get('city', '')
        if city:
            i = LinuxLocalTime.objects.all().count()
            if i == 0:
                object = LinuxLocalTime(city=city)
                object.save()
            else:
                object = LinuxLocalTime.objects.all().first()
                object.city = city
                object.save()
            ut = LinuxTools()
            ut.setLocaltimeEurope(city)
            return HttpResponseRedirect(reverse_lazy('app:main'))
        if update:
            root_parant = os.path.dirname(BASE_DIR)
            updater_path = os.path.join(BASE_DIR, 'updater.py')
            shutil.copy2(updater_path, root_parant)
            os.chdir(root_parant)
            exe_file = root_parant + "/updater.py"
            log_file = root_parant + "/update_log.txt"
            os.system("chmod 755 " + exe_file)
            os.system(exe_file + " > " + log_file + " 2>1 &")
        return HttpResponseRedirect(reverse_lazy('app:main'))

    def get_context_data(self, **kwargs):
        context = super(MainView, self).get_context_data(**kwargs)
        ut = LinuxTools()
        context['cities'] = ut.getCitiesEurope()
        context['city_selected'] = LinuxLocalTime.objects.all().first()
        pc = Pipcontroller()
        context['serial'] = pc.get_serial()
        # context['realtime'] = subprocess.check_output(["date", "+%D %H:%M"]).decode('ascii')
        return context


class StatusView(TemplateView):
    template_name = 'status.html'

    def post(self, request, *args, **kwargs):
        pc = Pipcontroller()
        if request.is_ajax():
            status = request.POST.get('status_request', '')
            status_xml = request.POST.get('status_xml', '')
            if status == "status":
                controller_status = pc.pipcontroller_running()
                watchdog_status = pc.pipwatchdog_running()
                return JsonResponse({'controller_status': controller_status, 'watchdog_status': watchdog_status})
            if status_xml:
                status_xml = pc.status()
                return HttpResponse(status_xml)
            else:
                return JsonResponse({'message': "Failed"})

        controller = request.POST.get('controller', '')
        watchdog = request.POST.get('watchdog', '')
        if controller == "start":
            pc.start()
        elif controller == "stop":
            pc.stop()
        if watchdog == "start":
            pc.start_pipwatchdog()
        elif watchdog == "stop":
            pc.kill_pipwatchdog()
        return HttpResponseRedirect(reverse_lazy('app:status'))

    def get_context_data(self, **kwargs):
        context = super(StatusView, self).get_context_data(**kwargs)
        pc = Pipcontroller()
        context['controller_status'] = pc.pipcontroller_running()
        context['watchdog_status'] = pc.pipwatchdog_running()
        context['status_xml'] = pc.status()
        return context


@method_decorator(csrf_exempt, name='dispatch')
class HeatingCircuitView(ListView):
    template_name = "heating_circuit.html"
    model = HeatingCircuit
    context_object_name = "circuits"
    paginate_by = 1

    def get_context_data(self, **kwargs):
        context = super(HeatingCircuitView, self).get_context_data(**kwargs)
        context['display_circuits'] = HeatingCircuit.objects.all()
        AI = AnalogInput.objects.filter(type=0)
        OWFS = OneWire.objects.all()
        VS = VirtualSensor.objects.all()
        DO_fixed = DigitalOutput.objects.all()
        VDO = VirtualDigitalOutput.objects.all()
        context['tsensors'] = list(chain(AI, OWFS, VS))
        context['DO'] = list(chain(DO_fixed, VDO))
        context['HZ'] = HeatingZone.objects.all()
        context['DHW'] = DomesticWater.objects.all()
        context['selected_ids'] = selected_dos()
        return context

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            command = request.POST.get('command', '')
            id = request.POST.get('id', '')
            circuits_in_json = request.POST.get('heating_circuit', '')

            if circuits_in_json:
                py_dict = json.loads(circuits_in_json)
                write_zones_to_ini(py_dict, parameter="circuit")

            if command == "add":
                object = HeatingCircuit(name="HC")
                object.save()
            elif command == "delete":
                HeatingCircuit.objects.get(id=int(id)).delete()

            return JsonResponse({'message': "Success"})


@method_decorator(csrf_exempt, name='dispatch')
class HeatingBoilerView(ListView):
    template_name = "heating_boiler.html"
    model = HeatingBoiler
    context_object_name = "boilers"
    paginate_by = 1

    def get_context_data(self, **kwargs):
        context = super(HeatingBoilerView, self).get_context_data(**kwargs)
        context['display_boilers'] = HeatingBoiler.objects.all()
        AI = AnalogInput.objects.filter(type=0)
        OWFS = OneWire.objects.all()
        VS = VirtualSensor.objects.all()
        DO_fixed = DigitalOutput.objects.all()
        VDO = VirtualDigitalOutput.objects.all()
        context['tsensors'] = list(chain(AI, OWFS, VS))
        context['DO'] = list(chain(DO_fixed, VDO))
        context['HZ'] = HeatingZone.objects.all()
        context['DHW'] = DomesticWater.objects.all()
        context['HC'] = HeatingCircuit.objects.all()
        context['selected_ids'] = selected_dos()
        return context

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            command = request.POST.get('command', '')
            id = request.POST.get('id', '')
            boilers_in_json = request.POST.get('heating_boiler', '')

            if boilers_in_json:
                py_dict = json.loads(boilers_in_json)
                write_zones_to_ini(py_dict, parameter="boiler")

            if command == "add":
                object = HeatingBoiler(name="HB")
                object.save()
            elif command == "delete":
                HeatingBoiler.objects.get(id=int(id)).delete()

            return JsonResponse({'message': "Success"})


