import os

from pipcwebmod.settings import BASE_DIR, PIPC_DIR
from pipweb.utils.sheme import sheme_to_db


def validate_first_char(line):
    if line[0] == "\n" or line[0] == "#" or line[0] == ";" or line[0] == "" or line[0] == " ":
        return False
    else:
        return True


def strip_strings_in_list(list):
    list = [string.split(":", 1)[1] for string in list]
    return list


def from_files_to_db():
    base_path = os.path.join(PIPC_DIR, 'dev')
    try:
        with open(os.path.join(base_path, 'hwd_info.txt'), "r") as file:
            content = []
            for line in file:
                if validate_first_char(line) == True:
                    content.append(line)
            content = [x.strip() for x in content]

            for x in content:
                if "type" in x:
                    type = x.split("=", 1)[1]

            if type == "Lx_1_2":
                AI = 4
                digital_outputs_count = 10
                AO = 4
            elif type == "Lx_1_2l":
                AI = 4
                digital_outputs_count = 4
                AO = 0
            elif type == "Lx_1":
                AI = 4
                digital_outputs_count = 12
                AO = 4
            elif type == "rasp" or type == "Rasp":
                AI = 0
                digital_outputs_count = 0
                AO = 0
    except:
        pass

    try:
        with open(os.path.join(base_path, 'adcsensors.ini'), "r") as file:
            content = []
            for line in file:
                if validate_first_char(line) == True:
                    content.append(line)
            content = [x.strip() for x in content]

            temp_count = 0
            pot_count = 0
            ai_type_list = []
            for x in content:
                if "type" in x:
                    if x == "type=potenciometer":
                        ai_type_list.append("potenciometer")
                        pot_count += 1
                    elif x == "type=temperature":
                        ai_type_list.append("temperature")
                        temp_count += 1
    except:
        pass

    try:
        with open(os.path.join(base_path, '1wire_0.ini'), "r") as file:
            content = []
            for line in file:
                if validate_first_char(line) == True:
                    content.append(line)
            content = [x.strip() for x in content]
            count_bus1 = len(content)

        with open(os.path.join(base_path, '1wire_1.ini'), "r") as file:
            content = []
            for line in file:
                if validate_first_char(line) == True:
                    content.append(line)
            content = [x.strip() for x in content]
            count_bus0 = len(content)
    except:
        pass

    try:
        with open(os.path.join(base_path, 'virtualio.ini'), "r") as file:
            content = []
            for line in file:
                if validate_first_char(line) == True:
                    content.append(line)
            content = [x.strip() for x in content]

            for x in content:
                if "T" in x:
                    T_virtual = int(x.split("=", 1)[1])
                elif "DO" in x:
                    DO_virtual = int(x.split("=", 1)[1])
                elif "AO" in x:
                    AO_virtual = int(x.split("=", 1)[1])
    except:
        pass

    try:
        with open(os.path.join(base_path, 'iostrings.txt'), "r") as file:
            content = []
            for line in file:
                if validate_first_char(line) == True:
                    content.append(line)
            content = [x.strip() for x in content]

            t_sensors_start = content.index("[TSENSORS]") + 1
            t_sensors_end = content.index("[/TSENSORS]")
            t_sensors_list = content[t_sensors_start:t_sensors_end]
            t_sensors_list = strip_strings_in_list(t_sensors_list)

            do_start = content.index("[DO]") + 1
            do_end = content.index("[/DO]")
            do_list = content[do_start:do_end]
            do_list = strip_strings_in_list(do_list)

            ao_start = content.index("[AO]") + 1
            ao_end = content.index("[/AO]")
            ao_list = content[ao_start:ao_end]
            if AO == 4:
                analog_outputs = strip_strings_in_list(ao_list)[:4]
                vao = strip_strings_in_list(ao_list)[4:]
            elif AO == 0 and AO_virtual > 0:
                analog_outputs = []
                vao = strip_strings_in_list(ao_list)
            if len(vao) == 0 and AO_virtual > 0:
                vao = ['' for i in range(AO_virtual)]

            di_start = content.index("[DI]") + 1
            di_end = content.index("[/DI]")
            di_list = content[di_start:di_end]
            digital_inputs = strip_strings_in_list(di_list)

            ai_start = content.index("[AI]") + 1
            ai_end = content.index("[/AI]")
            ai_list = content[ai_start:ai_end]
            ai_list = strip_strings_in_list(ai_list)

            analog_inputs = []
            i = 0
            for ai in ai_list:
                analog_inputs.append({'name': ai, 'type': ai_type_list[i]})
                i += 1

            virtual_sensors = t_sensors_list[temp_count + count_bus1 + count_bus0:]
            if len(virtual_sensors) == 0 and T_virtual > 0:
                virtual_sensors = ['' for i in range(T_virtual)]

            digital_outputs = do_list[:digital_outputs_count]
            vdo = do_list[digital_outputs_count:]
            if len(vdo) == 0 and DO_virtual > 0:
                vdo = ['' for i in range(DO_virtual)]

            sheme_to_db(digital_outputs, analog_inputs, analog_outputs, digital_inputs, vdo, virtual_sensors, vao)
    except:
        pass