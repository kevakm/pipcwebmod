import json
from datetime import datetime

from django.db import models
from pipctools import LinuxTools

HEATING_TYPES = (
    ('undefined', 'Undefined'),
    ('radiator', 'Radiator'),
    ('floor', 'Floor heating'),
)

CONTROL_TYPES = (
    ('undefined', 'Undefined'),
    ('valve', 'Zone valve'),
    ('circuit', 'Mixing Circuit'),
    ('aux', 'Aux'),
    ('no', 'No control'),
)

CTRL_TYPES = (
    ('undefined', 'Undefined'),
    ('bin', 'Bin'),
    ('pulsstep2', 'Pulsstep2'),
    ('pid', 'Pid'),
    ('p3w', 'P3w'),
    ('aux', 'Aux'),
    ('no', 'No control'),
)


class OneWire(models.Model):
    address = models.CharField(max_length=255)
    trim = models.FloatField(null=True, blank=True)
    name = models.CharField(max_length=255, null=True, blank=True)
    bus_id = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.address

    def get_model_name(self):
        return self.__class__.__name__


class DomesticWater(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    optimization = models.BooleanField(default=False)
    ts_id = models.IntegerField(blank=True, default=-1)
    ts_table = models.CharField(max_length=255, blank=True, null=True)
    fixed_temp = models.FloatField(blank=True, null=True)
    control_type = models.CharField(max_length=255, default='undefined', choices=CONTROL_TYPES)
    ctrl_type = models.CharField(max_length=255, default='undefined', choices=CTRL_TYPES)
    parameters = models.TextField(max_length=1024, null=True, blank=True, default={})

    def __str__(self):
        return self.name

    def get_model_name(self):
        return self.__class__.__name__

    def get_outid(self):
        dict = json.loads(self.parameters)
        id = dict.get('outID')
        if id['id'] != None:
            value = int(id['id'])
        else:
            value = id['id']
        return value

    def get_out_table(self):
        dict = json.loads(self.parameters)
        id = dict.get('outID')
        value = id['table']
        return value

    def is_valid(self):
        if self.ts_id == -1 or self.control_type == "undefined":
            return False
        if self.control_type == "valve" and self.get_outid() == -1:
            return False


class HeatingZone(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    heating = models.BooleanField(default=True)
    cooling = models.BooleanField(default=False)
    optimization = models.BooleanField(default=True)
    ts_id = models.IntegerField(blank=True, default=-1)
    ts_table = models.CharField(max_length=255, blank=True, null=True)
    sensor_pk = models.IntegerField(blank=True, default=-1)
    fixed_temp = models.FloatField(blank=True, null=True)
    heating_type = models.CharField(max_length=255, default='undefined', choices=HEATING_TYPES)
    control_type = models.CharField(max_length=255, default='undefined', choices=CONTROL_TYPES)
    ctrl_type = models.CharField(max_length=255, default='undefined', choices=CTRL_TYPES)
    parameters = models.TextField(max_length=1024, null=True, blank=True, default={})

    def __str__(self):
        return self.name

    def get_model_name(self):
        return self.__class__.__name__

    def get_outid(self):
        dict = json.loads(self.parameters)
        id = dict.get('outID')
        if id['id'] != None:
            value = int(id['id'])
        else:
            value = id['id']
        return value

    def get_out_table(self):
        dict = json.loads(self.parameters)
        id = dict.get('outID')
        value = id['table']
        return value

    def get_outOpenid(self):
        dict = json.loads(self.parameters)
        id = dict.get('outOpenID')
        if id['id'] != None:
            value = int(id['id'])
        else:
            value = id['id']
        return value

    def get_outOpen_table(self):
        dict = json.loads(self.parameters)
        id = dict.get('outOpenID')
        value = id['table']
        return value

    def get_outCloseid(self):
        dict = json.loads(self.parameters)
        id = dict.get('outCloseID')
        if id['id'] != None:
            value = int(id['id'])
        else:
            value = id['id']
        return value

    def get_outClose_table(self):
        dict = json.loads(self.parameters)
        id = dict.get('outCloseID')
        value = id['table']
        return value

    def get_tKesselID(self):
        dict = json.loads(self.parameters)
        id = dict.get('tKesselID')
        if id['id'] != None:
            value = int(id['id'])
        else:
            value = id['id']
        return value

    def get_tKesselID_table(self):
        dict = json.loads(self.parameters)
        id = dict.get('tKesselID')
        value = id['table']
        return value

    def get_tFloorID(self):
        dict = json.loads(self.parameters)
        id = dict.get('tFloorID')
        if id['id'] != None:
            value = int(id['id'])
        else:
            value = id['id']
        return value

    def get_tFloorID_table(self):
        dict = json.loads(self.parameters)
        id = dict.get('tFloorID')
        value = id['table']
        return value

    def is_valid(self):
        if self.ts_id == -1 or self.control_type == "undefined":
            return False
        if (self.ctrl_type == "bin" or self.ctrl_type == "pulsstep2" or self.ctrl_type == "pid") and self.get_outid() == -1:
            return False
        if self.control_type == "circuit" and (self.get_outid() == -1 or self.get_outOpenid() == -1 or self.get_outCloseid() == -1):
            return False


class ModbusServer(models.Model):
    enable = models.BooleanField(default=True)
    read_only = models.BooleanField(default=True)
    port = models.IntegerField(default=502)

    def get_model_name(self):
        return self.__class__.__name__

    # class Meta:
    #     verbose_name = 'Modbus'
    #     verbose_name_plural = 'Modbuses'


class DigitalOutput(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    def get_model_name(self):
        return self.__class__.__name__


class DigitalInput(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    def get_model_name(self):
        return self.__class__.__name__


ANALOG_INPUTS = (
    ('temperature', 'Temperature'),
    ('potenciometer', 'Potenciometer'),
)

PARA_TYPES = (
    ('undefined', 'Undefined'),
    ('pt100', 'PT100'),
    ('pt1000', 'PT1000'),
    ('pt500', 'PT500'),
    ('custom', 'CUSTOM'),
)

class AnalogInput(models.Model):
    name = models.CharField(max_length=255)
    type = models.CharField(max_length=255, default='temperature', choices=ANALOG_INPUTS)
    para_type = models.CharField(max_length=255, default='undefined', choices=PARA_TYPES)
    parameters = models.TextField(max_length=1024, null=True, blank=True)

    def __str__(self):
        return self.name

    def get_model_name(self):
        return self.__class__.__name__


class AnalogOutput(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    def get_model_name(self):
        return self.__class__.__name__


class VirtualDigitalOutput(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    def get_model_name(self):
        return self.__class__.__name__


class VirtualSensor(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    def get_model_name(self):
        return self.__class__.__name__


class VirtualAnalogOutput(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

    def get_model_name(self):
        return self.__class__.__name__


class LinuxLocalTime(models.Model):
    city = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.city


CIRCUIT_TYPES = (
    ('undefined', 'Undefined'),
    ('direct', 'Direct'),
    ('mix', 'Mix'),
)


class HeatingCircuit(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    ts_id = models.IntegerField(blank=True, default=-1)
    ts_table = models.CharField(max_length=255, blank=True, null=True)
    circuit_type = models.CharField(max_length=255, default='undefined', choices=CIRCUIT_TYPES)
    ctrl_type = models.CharField(max_length=255, default='undefined', choices=CTRL_TYPES)
    hz = models.ManyToManyField(HeatingZone, blank=True, related_name="circuits_zones")
    dhw = models.ManyToManyField(DomesticWater, blank=True, related_name="circuits_waters")
    parameters = models.TextField(max_length=1024, null=True, blank=True, default={})

    def __str__(self):
        return self.name

    def get_model_name(self):
        return self.__class__.__name__

    def get_outid(self):
        dict = json.loads(self.parameters)
        id = dict.get('outID')
        if id['id'] != None:
            value = int(id['id'])
        else:
            value = id['id']
        return value

    def get_out_table(self):
        dict = json.loads(self.parameters)
        id = dict.get('outID')
        value = id['table']
        return value

    def get_outOpenid(self):
        dict = json.loads(self.parameters)
        id = dict.get('outOpenID')
        if id['id'] != None:
            value = int(id['id'])
        else:
            value = id['id']
        return value

    def get_outOpen_table(self):
        dict = json.loads(self.parameters)
        id = dict.get('outOpenID')
        value = id['table']
        return value

    def get_outCloseid(self):
        dict = json.loads(self.parameters)
        id = dict.get('outCloseID')
        if id['id'] != None:
            value = int(id['id'])
        else:
            value = id['id']
        return value

    def get_outClose_table(self):
        dict = json.loads(self.parameters)
        id = dict.get('outCloseID')
        value = id['table']
        return value

    def is_valid(self):
        if self.circuit_type == "direct" and self.get_outid() == -1:
            return False
        if self.circuit_type == "mix" and (self.ts_id == -1 or self.ctrl_type == "undefined"):
            return False
        if self.circuit_type == "mix" and self.ctrl_type == "p3w" and \
                (self.get_outid() == -1 or self.get_outOpenid() == -1 or self.get_outCloseid() == -1):
            return False


class HeatingBoiler(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    ts_id = models.IntegerField(blank=True, default=-1)
    ts_table = models.CharField(max_length=255, blank=True, null=True)
    boiler_type = models.CharField(max_length=255, default='undefined', choices=CIRCUIT_TYPES)
    ctrl_type = models.CharField(max_length=255, default='undefined', choices=CTRL_TYPES)
    hz = models.ManyToManyField(HeatingZone, blank=True, related_name="boilers_zones")
    dhw = models.ManyToManyField(DomesticWater, blank=True, related_name="boilers_waters")
    hc = models.ManyToManyField(HeatingCircuit, blank=True, related_name="boilers_circuits")
    parameters = models.TextField(max_length=1024, null=True, blank=True, default={})

    def __str__(self):
        return self.name

    def get_model_name(self):
        return self.__class__.__name__

    def get_outid(self):
        dict = json.loads(self.parameters)
        id = dict.get('outID')
        if id['id'] != None:
            value = int(id['id'])
        else:
            value = id['id']
        return value

    def get_out_table(self):
        dict = json.loads(self.parameters)
        id = dict.get('outID')
        value = id['table']
        return value

    def get_outOpenid(self):
        dict = json.loads(self.parameters)
        id = dict.get('outOpenID')
        if id['id'] != None:
            value = int(id['id'])
        else:
            value = id['id']
        return value

    def get_outOpen_table(self):
        dict = json.loads(self.parameters)
        id = dict.get('outOpenID')
        value = id['table']
        return value

    def get_outCloseid(self):
        dict = json.loads(self.parameters)
        id = dict.get('outCloseID')
        if id['id'] != None:
            value = int(id['id'])
        else:
            value = id['id']
        return value

    def get_outClose_table(self):
        dict = json.loads(self.parameters)
        id = dict.get('outCloseID')
        value = id['table']
        return value

    def is_valid(self):
        if self.boiler_type == "direct" and self.get_outid() == -1:
            return False
        if self.boiler_type == "mix" and (self.ts_id == -1 or self.ctrl_type == "undefined"):
            return False
        if self.boiler_typee == "mix" and self.ctrl_type == "p3w" and \
                (self.get_outid() == -1 or self.get_outOpenid() == -1 or self.get_outCloseid() == -1):
            return False


class Updater(models.Model):
    current_gui = models.FloatField(blank=True, null=True)
    latest_gui = models.FloatField(blank=True, null=True)
    current_app = models.FloatField(default=0.0, blank=True, null=True)
    latest_app = models.FloatField(default=0.0, blank=True, null=True)
    update_gui = models.BooleanField(default=False)
    update_app = models.BooleanField(default=False)


class ModbusDevice(models.Model):
    name = models.CharField(max_length=255)
    ip_address = models.CharField(max_length=255, blank=True, null=True)
    slave_address = models.CharField(max_length=255, blank=True, null=True)
    port = models.IntegerField(default=502)

    def __str__(self):
        return self.name

MODBUS_TYPES = (
    ('undefined', 'Undefined'),
    ('do', 'Digital output'),
    ('di', 'Digital input'),
    ('ao', 'Analog output'),
    ('ai', 'Analog input'),
    ('ts', 'Temperature sensor'),
)

WORD_TYPES = (
    ('undefined', 'Undefined'),
    ('float', 'Float little endian'),
)

INI_POLICY = (
    ('recommended', 'Recommended'),
    ('required', 'Required'),
    ('no_init', 'No init'),
)

REGISTERS = (
    ('undefined', 'Undefined'),
    ('coils', 'Coils'),
    ('discrete_inputs', 'Discrete Inputs'),
    ('holding_registers', 'Holding Registers'),
    ('input_registers', 'Input Registers'),
)


class ModbusCard(models.Model):
    name = models.CharField(max_length=255)
    type = models.CharField(max_length=255, default='undefined', choices=MODBUS_TYPES)
    registers = models.CharField(max_length=255, default='undefined', choices=REGISTERS)
    word_type = models.CharField(max_length=255, default='float', choices=WORD_TYPES)
    update_time = models.IntegerField(default=10)
    address = models.IntegerField(blank=True, null=True)
    no_elements = models.IntegerField(default=1, blank=True, null=True)
    ini_policy = models.CharField(max_length=255, default='recommended', choices=INI_POLICY)
    device = models.ForeignKey(ModbusDevice, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Modbus(models.Model):
    name = models.CharField(max_length=255)
    type = models.CharField(max_length=255, default='undefined', choices=MODBUS_TYPES)
    card = models.ForeignKey(ModbusCard, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.name

    def get_type(self):
        return self.type

    def get_model_name(self):
        return self.__class__.__name__


class ProgressBar(models.Model):
    percentage = models.IntegerField(default=0)
