{% extends "base.html" %}
{% load staticfiles %}
{% load get_parameter %}

{% block head %}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
{% endblock %}

{% block content %}
    <div class="container">
        <div class="row">
            <div class="col-md-3 list-group">
                <h3>Heating Circuits</h3>
                {% for circuit in display_circuits %}
                    <a class="cancela" href="{% url 'app:heating-circuits' %}?page={{ forloop.counter }}">
                    <ul circuit-id="{{ forloop.counter }}" db-id="{{ circuit.id }}" class="list-group-item">
                        <li style="display:inline-block;">{% if circuit.is_valid == False %}<span class="glyphicon glyphicon-alert"></span>{% endif %} {{ circuit }}</li>
                        <li style="display:inline-block;" class="pull-right">
                            <button type="submit" circuit-id="{{ forloop.counter }}" db-id="{{ circuit.id }}" class="btn-delete-circuit"></button>
                        </li>
                    </ul>
                    </a>
                {% endfor %}
                <br>
                <button id="add_circuit" type="submit" class="btn btn-default">add</button>
            </div>
            <div id="main-content" class="col-md-9 aliceblue">
                {% for circuit in circuits %}
                    <div db-id="{{ circuit.id }}" class="row circuit-main">
                        <div class="row">
                            <div class="col-sm-4 col-sm-offset-4 col-xs-6 col-xs-offset-3">
                                <h3 class="text-center">Circuit Name</h3>
                                <input type="text" class="form-control" name="heating_circuit" value="{{ circuit }}">
                            </div>
                        </div>
                        <h3 class="text-center">Circuit Type</h3>
                        <div class="col-sm-2 col-sm-offset-4 col-xs-3 col-xs-offset-3">
                            <label>
                                <input type="radio" name="circuit_type{{ forloop.counter0 }}" value="direct"
                                {% if circuit.circuit_type == "direct" or circuit.circuit_type == "undefined" %}checked="checked"{% endif %}>
                                <img src="{% static '/img/direct.png' %}" class="img-responsive">
                            </label>
                        </div>
                        <div class="col-sm-2 col-xs-3">
                            <label>
                                <input type="radio" name="circuit_type{{ forloop.counter0 }}" value="mix"
                                {% if circuit.circuit_type == "mix" %}checked="checked"{% endif %}>
                                <img src="{% static '/img/mix.png' %}" class="img-responsive">
                            </label>
                        </div>
                        <div class="col-sm-6 col-sm-offset-3 direct-content" {% if circuit.circuit_type == "mix" %}style="display: none;" {% endif %}>
                            <p class="text-center"><b>Connect heating zones</b></p>
                            <select class="direct-hz-input form-control" multiple="multiple" style="width:408px;">
                                {% for zone in HZ %}
                                <option db-id="{{ zone.id }}" {% for z in circuit.hz.all %}{% if z.id == zone.id %}selected="selected"{% endif %}{% endfor %}>{{ forloop.counter0 }} {{ zone }}</option>
                                {% endfor %}
                            </select>
                            <p class="text-center"><b>Connect domestic water</b></p>
                            <select class="direct-dhw-input form-control" multiple="multiple" style="width:408px;">
                                {% for water in DHW %}
                                <option db-id="{{ water.id }}" {% for w in circuit.dhw.all %}{% if w.id == water.id %}selected="selected"{% endif %}{% endfor %}>{{ forloop.counter0 }} {{ water }}</option>
                                {% endfor %}
                            </select>
                            <p class="text-center"><b>Digital output</b></p>
                            <select class="bin_outID form-control output-select">
                                <option value="-1" table-name="">Select an option</option>
                                {% for output in DO %}
                                    <option value="{{ output.id }}" table-name="{{ output.get_model_name }}"
                                            name="{{ output.name }}"
                                            {% if circuit.get_outid == output.id and circuit.get_out_table == output.get_model_name %}selected="selected"{% else %}
                                                {% for id in selected_ids %}{% if output.id == id.0 and output.get_model_name == id.1 %}class="disabled"{% endif %}{% endfor %}
                                            {% endif %}>
                                        [{{ forloop.counter0 }}] {{ output.name }}</option>
                                {% endfor %}
                            </select>
                        </div>
                        <div class="col-md-6 col-md-offset-3 mix-content" {% if circuit.circuit_type != "mix" %}style="display: none;"{% endif %}>
                            <p class="text-center"><b>Temperature Sensor</b></p>
                            <select class="tsensors_select form-control">
                                <option value="-1" table-name="">Select an option</option>
                                {% for sensor in tsensors %}
                                    <option value="{{ sensor.id }}" table-name="{{ sensor|to_class_name }}"
                                            {% if circuit.ts_table == sensor.get_model_name and circuit.ts_id == sensor.id %}selected="selected"{% endif %}>
                                        [{{ forloop.counter0 }} {{ sensor|to_class_name }} ] {{ sensor.name }}</option>
                                {% endfor %}
                            </select>
                            <br>
                            <p class="text-center"><b>Heating curve</b></p>
                            <img src="{% static '/img/wcurve.png' %}" class="img-responsive">
                            <table>
                                <ol type="A">
                                    <li>Underfloor heating system, slope 0.2 to 0.8</li>
                                    <li>Low temperature heating system, slope 0.8 to 1.6</li>
                                    <li>Heating system with a boiler water temperature in excess of 167°F (75°C), slope1.6 to 2.0</li>
                                </ol>
                                <tr>
{#                                    <td>wcurveSlope</td>#}
                                    <td>Slope</td>
                                    <td>
                                        <input type="number" min="0.2" max="3.4" step="0.1" class="wcurveSlope form-control"
                                        {% if circuit.parameters|get_value:"wcurveSlope" %}value={{ circuit.parameters|get_value:"wcurveSlope" }}{% else %}value="0.5"{% endif %}>
                                    </td>
                                </tr>
                                <tr>
{#                                    <td>wcurveShift</td>#}
                                    <td>Shift</td>
                                    <td>
                                        <input type="number" min="-10" max="10" step="0.1" value="0" class="wcurveShift form-control"
                                        {% if circuit.parameters|get_value:"wcurveShift" %}value={{ circuit.parameters|get_value:"wcurveShift" }}{% endif %}>
                                    </td>
                                </tr>
                                <tr>
{#                                    <td>wcurve_max</td>#}
                                    <td>Weather curve max</td>
                                    <td>
                                        <input type="number" max="90" step="0.1" class="wcurve_max form-control"
                                        {% if circuit.parameters|get_value:"wcurve_max" %}value={{ circuit.parameters|get_value:"wcurve_max" }}{% else %}value="70.0"{% endif %}>
                                    </td>
                                </tr>
                                <tr>
{#                                    <td>wcurve_min</td>#}
                                    <td>Weather curve min</td>
                                    <td width="100">
                                        <input type="number" min="20" step="0.1" class="wcurve_min form-control"
                                        {% if circuit.parameters|get_value:"wcurve_min" %}value={{ circuit.parameters|get_value:"wcurve_min" }}{% else %}value="25.0"{% endif %}>
                                    </td>
                                </tr>
                            </table>
                            <br>
                            <p class="text-center"><b>Connect heating zones</b></p>
                            <select class="mix-hz-input form-control" multiple="multiple" style="width:408px;">
                                {% for zone in HZ %}
                                <option db-id="{{ zone.id }}" {% for z in circuit.hz.all %}{% if z.id == zone.id %}selected="selected"{% endif %}{% endfor %}>{{ forloop.counter0 }} {{ zone }}</option>
                                {% endfor %}
                            </select>
                            <p class="text-center"><b>Connect domestic water</b></p>
                            <select class="mix-dhw-input form-control" multiple="multiple" style="width:408px;">
                                {% for water in DHW %}
                                <option db-id="{{ water.id }}" {% for w in circuit.dhw.all %}{% if w.id == water.id %}selected="selected"{% endif %}{% endfor %}>{{ forloop.counter0 }} {{ water }}</option>
                                {% endfor %}
                            </select>
                            <br>
                            <br>
                            <div class="control_type row">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <label>
                                        <input type="radio" name="mix_options{{ forloop.counter0 }}"
                                               value="p3w" {% if circuit.ctrl_type == "p3w" %}checked="checked"{% endif %}>
                                        <img src="{% static '/img/zone_circuit.png' %}" class="img-responsive">
                                    </label>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <label>
                                        <input type="radio" name="mix_options{{ forloop.counter0 }}"
                                               value="aux" {% if circuit.ctrl_type == "aux" %}checked="checked"{% endif %}>
                                        <img src="{% static '/img/aux_type.png' %}" class="img-responsive">
                                    </label>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <label>
                                        <input type="radio" name="mix_options{{ forloop.counter0 }}"
                                               value="no" {% if circuit.ctrl_type == "no" %}checked="checked"{% endif %}>
                                        <img src="{% static '/img/no_type.png' %}" class="img-responsive">
                                    </label>
                                </div>
                            </div>
                            <div class="control_type_content" {% if circuit.ctrl_type != "p3w" %}style="display: none;"{% endif %}>
                                <div class="p3w-content">
                                    <table id="p3w_table">
                                        <tr>
                                            <th>Connections</th>
                                        </tr>
                                        <tr>
{#                                            <td>outID</td>#}
                                            <td>Pump digital output id</td>
                                            <td><select class="p3w_outID form-control output-select p3w_select">
                                                <option value="-1" table-name="">Select an option</option>
                                                {% for output in DO %}
                                                    <option value="{{ output.id }}" table-name="{{ output.get_model_name }}" name="{{ output.name }}"
                                                            {% if circuit.get_outid == output.id and circuit.get_out_table == output.get_model_name %}selected="selected"{% else %}
                                                                {% for id in selected_ids %}{% if output.id == id.0 and output.get_model_name == id.1 %}class="disabled"{% endif %}{% endfor %}
                                                            {% endif %}>
                                                        [{{ forloop.counter0 }}] {{ output.name }}</option>
                                                {% endfor %}
                                            </select></td>
                                        </tr>
                                        <tr>
{#                                            <td>outOpenID</td>#}
                                            <td>Mixing valve open (digital output id)</td>
                                            <td><select class="outOpenID form-control output-select p3w_select">
                                                <option value="-1" table-name="">Select an option</option>
                                                {% for output in DO %}
                                                    <option value="{{ output.id }}" table-name="{{ output.get_model_name }}" name="{{ output.name }}"
                                                            {% if circuit.get_outOpenid == output.id and circuit.get_outOpen_table == output.get_model_name %}selected="selected"{% else %}
                                                            {% for id in selected_ids %}{% if output.id == id.0 and output.get_model_name == id.1 %}class="disabled"
                                                            {% endif %}{% endfor %}
                                                            {% endif %}>
                                                        [{{ forloop.counter0 }}] {{ output.name }}</option>
                                                {% endfor %}
                                            </select></td>
                                        </tr>
                                        <tr>
{#                                            <td>outCloseID</td>#}
                                            <td>Mixing valve close (digital output id)</td>
                                            <td><select class="outCloseID form-control output-select p3w_select">
                                                <option value="-1" table-name="">Select an option</option>
                                                {% for output in DO %}
                                                    <option value="{{ output.id }}"
                                                            table-name="{{ output.get_model_name }}"
                                                            name="{{ output.name }}"
                                                            {% if circuit.get_outCloseid == output.id and circuit.get_outClose_table == output.get_model_name %}selected="selected"{% else %}
                                                            {% for id in selected_ids %}{% if output.id == id.0 and output.get_model_name == id.1 %}class="disabled"
                                                            {% endif %}{% endfor %}
                                                            {% endif %}>
                                                        [{{ forloop.counter0 }}] {{ output.name }}</option>
                                                {% endfor %}
                                            </select></td>
                                        </tr>
                                        <tr>
                                            <th>Advanced settings</th>
                                        </tr>
                                        <tr>
{#                                            <td>roomGain</td>#}
                                            <td>Temperature correction (2 – floor heating, 8 – radiators)
                                            </td>
                                            <td><input type="number" min="0" step="0.1"
                                                       class="roomGain form-control"
                                                       {% if circuit.parameters|get_value:"roomGain" %}value=
                                                           {{ circuit.parameters|get_value:"roomGain" }}{% else %}value="0.0"{% endif %}>
                                            </td>
                                        </tr>
                                        <tr>
{#                                            <td>weatherGain</td>#}
                                            <td>Outdoor temperature gain</td>
                                            <td><input type="number" min="0" step="0.1"
                                                       class="weatherGain form-control"
                                                       {% if circuit.parameters|get_value:"weatherGain" %}value=
                                                           {{ circuit.parameters|get_value:"weatherGain" }}{% else %}value="10.0"{% endif %}>
                                            </td>
                                        </tr>
                                        <tr>
{#                                            <td>dt_room_max</td>#}
                                            <td>Maximum temperature difference room–set (above this value
                                                heating is disabled)
                                            </td>
                                            <td><input type="number" min="0" step="0.1"
                                                       class="dt_room_max form-control"
                                                       {% if circuit.parameters|get_value:"dt_room_max" %}value=
                                                           {{ circuit.parameters|get_value:"dt_room_max" }}{% else %}value="0.0"{% endif %}>
                                            </td>
                                        </tr>
                                        <tr>
{#                                            <td>weathCHrs</td>#}
                                            <td>Outdoor hours average</td>
                                            <td><input type="number" min="0" class="weathCHrs form-control"
                                                       {% if circuit.parameters|get_value:"weathCHrs" %}value=
                                                           {{ circuit.parameters|get_value:"weathCHrs" }}{% else %}value="10"{% endif %}>
                                            </td>
                                        </tr>
                                        <tr>
{#                                            <td>waitTime</td>#}
                                            <td>Valve sleep_time in seconds</td>
                                            <td><input type="number" min="1"
                                                       class="p3w_waitTime form-control"
                                                       {% if circuit.parameters|get_value:"p3w_waitTime" %}value=
                                                           {{ circuit.parameters|get_value:"p3w_waitTime" }}{% else %}value="120"{% endif %}>
                                            </td>
                                        </tr>
                                        <tr>
{#                                            <td>K</td>#}
                                            <td>Valve travel time at dt=1K diff in miliseconds (proportional
                                                gain)
                                            </td>
                                            <td><input type="number" min="100" class="K form-control"
                                                       {% if circuit.parameters|get_value:"K" %}value=
                                                           {{ circuit.parameters|get_value:"K" }}{% else %}value="500"{% endif %}>
                                            </td>
                                        </tr>
                                        <tr>
{#                                            <td>K max</td>#}
                                            <td></td>
                                            <td><input type="number" min="1000" class="Kmax form-control"
                                                       {% if circuit.parameters|get_value:"Kmax" %}value=
                                                           {{ circuit.parameters|get_value:"Kmax" }}{% else %}value="10000"{% endif %}>
                                            </td>
                                        </tr>
                                        <tr>
{#                                            <td>K min</td>#}
                                            <td></td>
                                            <td><input type="number" min="100" class="Kmin form-control"
                                                       {% if circuit.parameters|get_value:"Kmin" %}value=
                                                           {{ circuit.parameters|get_value:"Kmin" }}{% else %}value="1000"{% endif %}>
                                            </td>
                                        </tr>
                                        <tr>
{#                                            <td>K_D</td>#}
                                            <td>Travel factor (differential gain)</td>
                                            <td><input type="number" min="100" class="K_D form-control"
                                                       {% if circuit.parameters|get_value:"K_D" %}value=
                                                           {{ circuit.parameters|get_value:"K_D" }}{% else %}value="5000"{% endif %}>
                                            </td>
                                        </tr>
                                        <tr>
{#                                            <td>pdt_trig</td>#}
                                            <td>Travel proportional trigger temperature difference</td>
                                            <td><input type="number" min="0" step="0.1"
                                                       class="pdt_trig form-control"
                                                       {% if circuit.parameters|get_value:"pdt_trig" %}value=
                                                           {{ circuit.parameters|get_value:"pdt_trig" }}{% else %}value="1.0"{% endif %}>
                                            </td>
                                        </tr>
                                        <tr>
{#                                            <td>ddt_trig</td>#}
                                            <td>Travel differential trigger temperature difference</td>
                                            <td><input type="number" min="0" step="0.1"
                                                       class="ddt_trig form-control"
                                                       {% if circuit.parameters|get_value:"ddt_trig" %}value=
                                                           {{ circuit.parameters|get_value:"ddt_trig" }}{% else %}value="0.1"{% endif %}>
                                            </td>
                                        </tr>
                                        <tr>
{#                                            <td>tKesselID</td>#}
                                            <td>Heat generator sensor id</td>
                                            <td><select class="tKesselID form-control">
                                                <option value="-1">None</option>
                                                {% for sensor in tsensors %}
                                                    <option value="{{ sensor.id }}">
                                                        [{{ forloop.counter0 }}] {{ sensor.name }}</option>
                                                {% endfor %}
                                            </select></td>
                                        </tr>
                                        <tr style="display: none;">
{#                                            <td>tKesselMin</td>#}
                                            <td>Minimum temperature of heat generator (below this value
                                                heating is disabled)
                                            </td>
                                            <td><input type="number" step="0.1"
                                                       class="tKesselMin form-control"
                                                       {% if circuit.parameters|get_value:"tKesselMin" %}value=
                                                           {{ circuit.parameters|get_value:"tKesselMin" }}{% else %}value="50.0"{% endif %}>
                                            </td>
                                        </tr>
                                        <tr>
{#                                            <td>tFloorID</td>#}
                                            <td>Floor heating sensor id</td>
                                            <td><select class="tFloorID form-control">
                                                <option value="-1">None</option>
                                                {% for sensor in tsensors %}
                                                    <option value="{{ sensor.id }}">
                                                        [{{ forloop.counter0 }}] {{ sensor.name }}</option>
                                                {% endfor %}
                                            </select></td>
                                        </tr>
                                        <tr style="display: none;">
{#                                            <td>tFloorMax</td>#}
                                            <td>Maximum temperature of floor heating (above this value
                                                heating is disabled)
                                            </td>
                                            <td><input type="number" step="0.1"
                                                       class="tFloorMax form-control"
                                                       {% if circuit.parameters|get_value:"tFloorMax" %}value=
                                                           {{ circuit.parameters|get_value:"tFloorMax" }}{% endif %}>
                                            </td>
                                        </tr>
                                        <tr>
{#                                            <td>tOutMax</td>#}
                                            <td>Maximum outdoor temperature (above this value heating is
                                                disabled)
                                            </td>
                                            <td><input type="number" step="0.1" class="tOutMax form-control"
                                                       {% if circuit.parameters|get_value:"tOutMax" %}value=
                                                           {{ circuit.parameters|get_value:"tOutMax" }}{% else %}value="20.0"{% endif %}>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                {% endfor %}
            </div>
        </div>
    </div>
{% endblock %}

{% block customscripts %}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script>
        function getCookie(name) {
            var cookieValue = null;
            if (document.cookie && document.cookie != '') {
                var cookies = document.cookie.split(';');
                for (var i = 0; i < cookies.length; i++) {
                    var cookie = jQuery.trim(cookies[i]);
                    if (cookie.substring(0, name.length + 1) == (name + '=')) {
                        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                        break;
                    }
                }
            }
            return cookieValue;
        }

        $(document).ready(function () {
            var parameter = window.location.search;
            if (parameter == "") {
                $("ul.list-group-item:first").addClass('active')
            } else {
                var page_no = parameter.split("=")[1];
                if ($.isNumeric(page_no)) {
                    $("ul.list-group-item[circuit-id=" + page_no + "]").addClass('active')
                } else {
                    $('ul.list-group-item:last').addClass('active')
                }
            }
            $(".direct-hz-input").select2();
            $(".direct-dhw-input").select2();
            $(".mix-hz-input").select2();
            $(".mix-dhw-input").select2();
        });

        $('input[name^="circuit_type"]').click(function () {
            var type = $(this).val();
            var $wrapper = $(this).closest('div.circuit-main');
            if (type == "direct") {
                $wrapper.find('.direct-content').show();
                $wrapper.find('.mix-content').hide();
            } else if (type == "mix") {
                $wrapper.find('.mix-content').show();
                $wrapper.find('.direct-content').hide();
            }
        });

        $('input[name^="mix_options"]').click(function () {
            var option = $(this).val();
            var $wrapper = $(this).closest('div.control_type');
            if (option.startsWith("p3w")) {
                $wrapper.next('div.control_type_content').show();
            } else {
                $wrapper.next('div.control_type_content').hide();
            }
        });


        $('.tKesselID').change(function () {
            var val = $(this).val();
            if (val != -1) {
                $(this).closest('tr').next().show();
            } else {
                $(this).closest('tr').next().hide()
            }
        });

        $('.tFloorID').change(function () {
            var val = $(this).val();
            if (val != -1) {
                $(this).closest('tr').next().show();
            } else {
                $(this).closest('tr').next().hide()
            }
        });

        jQuery(function () {
            $('.tKesselID').change();
            $('.tFloorID').change();
        });

        var prevSelected = ""

        $('select.output-select').focus(function () {
            prevSelected = $(this).val();
        });

        var optionSelectedVal = ""

        $('select.output-select').change(function () {
            var optionSelected = $(this).find('option:selected');
            optionSelectedVal = optionSelected.val();
            if (optionSelected.hasClass('disabled')) {
                var name = optionSelected.attr('name');
                $(this).find('option:first').prop('selected', true);
                $(this).find("option[value=" + prevSelected + "]").removeClass('disabled');
                $(this).blur()
            } else {
                $(this).blur()
            }
        });

        $('select.p3w_select').change(function () {
            $('select.p3w_select').each(function () {
                $(this).find("option[value=" + prevSelected + "]").removeClass('disabled');
                if (optionSelectedVal != -1) {
                    $(this).find("option[value=" + optionSelectedVal + "]").addClass('disabled');
                }
            });
        });

        function saveCircuit(options) {
            var heating_circuit = [];
            var object = {};
            var parameters = {};
            var csrftoken = getCookie('csrftoken');

            var $wrapper = $('.circuit-main');
            object["id"] = $wrapper.attr('db-id');
            object["name"] = $wrapper.find('input[name="heating_circuit"]').val();

            object["ts_id"] = null;
            object["ts_table"] = null;
            object["circuit_type"] = null;
            object["ctrl_type"] = null;
            var dhw = [];
            var hz = [];

            var circuit_type = $wrapper.find('input[name^="circuit_type"]:checked').val();
            if (circuit_type == "direct") {
                object["circuit_type"] = circuit_type;
                $(".direct-hz-input :selected").each(function () {
                    hz.push($(this).attr('db-id'));
                });
                $(".direct-dhw-input :selected").each(function () {
                    dhw.push($(this).attr('db-id'));
                });
                var outID = {};
                outID['id'] = $wrapper.find('.bin_outID').val();
                outID['table'] = $wrapper.find('.bin_outID').children(':selected').attr('table-name');
                parameters['outID'] = outID
            } else if (circuit_type == "mix") {
                object["circuit_type"] = circuit_type;
                object["ts_id"] = $wrapper.find('.tsensors_select').val();
                object["ts_table"] = $wrapper.find('.tsensors_select').children(':selected').attr('table-name');
                $(".mix-hz-input :selected").each(function () {
                    hz.push($(this).attr('db-id'));
                });
                $(".mix-dhw-input :selected").each(function () {
                    dhw.push($(this).attr('db-id'));
                });
                parameters["wcurveSlope"] = $wrapper.find('.wcurveSlope').val();
                parameters["wcurveShift"] = $wrapper.find('.wcurveShift').val();
                parameters["wcurve_max"] = $wrapper.find('.wcurve_max').val();
                parameters["wcurve_min"] = $wrapper.find('.wcurve_min').val();
                var ctrl_type = $wrapper.find('input[name^="mix_options"]:checked').val();
                object["ctrl_type"] = ctrl_type;
                if (ctrl_type == "p3w"){
                    var outID = {};
                    outID['id'] = $wrapper.find('.p3w_outID').val();
                    outID['table'] = $wrapper.find('.p3w_outID').children(':selected').attr('table-name');
                    parameters['outID'] = outID;
                    var outOpenID = {};
                    outOpenID['id'] = $wrapper.find('.outOpenID').val();
                    outOpenID['table'] = $wrapper.find('.outOpenID').children(':selected').attr('table-name');
                    parameters['outOpenID'] = outOpenID
                    var outCloseID = {};
                    outCloseID['id'] = $wrapper.find('.outCloseID').val();
                    outCloseID['table'] = $wrapper.find('.outCloseID').children(':selected').attr('table-name');
                    parameters['outCloseID'] = outCloseID
                    parameters["roomGain"] = $wrapper.find('.roomGain').val();
                    parameters["weatherGain"] = $wrapper.find('.weatherGain').val();
                    parameters["dt_room_max"] = $wrapper.find('.dt_room_max').val();
                    parameters["weathCHrs"] = $wrapper.find('.weathCHrs').val();
                    parameters["waitTime"] = $wrapper.find('.p3w_waittime').val();
                    parameters["K"] =$wrapper.find('.K').val();
                    parameters["Kmax"] = $wrapper.find('.Kmax').val();
                    parameters["Kmin"] = $wrapper.find('.Kmin').val();
                    parameters["K_D"] = $wrapper.find('.K_D').val();
                    parameters["pdt_tring"] = $wrapper.find('.pdt_trig').val();
                    parameters["ddt_tring"] = $wrapper.find('.ddt_trig').val();
                    parameters["tKesselID"] = $wrapper.find('.tKesselID').val();
                    parameters["tFloorID"] = $wrapper.find('.tFloorID').val();
                    parameters["tOutMax"] = $wrapper.find('.tOutMax').val();
                    if (parameters['tKesselID'] != -1) {
                        parameters['tKesselMin'] = $wrapper.find('.tKesselMin').val();
                    } else {
                        parameters['tKesselMin'] = -1
                    }
                    if (parameters['tFloorID'] != -1) {
                        parameters['tFloorMax'] = $wrapper.find('.tFloorMax').val();
                    } else {
                        parameters['tFloorMax'] = -1
                    }
                }
            }
            object["hz"] = hz;
            object["dhw"] = dhw;
            object["parameters"] = parameters;
            heating_circuit.push(object);

            $.ajax({
                url: '{% url 'app:heating-circuits' %}',
                type: 'POST',
                data: {
                    heating_circuit: JSON.stringify(heating_circuit),
                    csrfmiddlewaretoken: csrftoken,
                },
                dataType: 'json',
                complete: function (data) {
                    //debugMsg(data);
                },
                success: function (data) {
                    if (options && options.onSuccess) {
                        options.onSuccess();
                    }
                },
            });
        }

        function addDeleteHc(command, id, link) {
            var csrftoken = getCookie('csrftoken');

            $.ajax({
                url: '{% url 'app:heating-circuits' %}',
                type: 'POST',
                data: {
                    command: command,
                    id: id,
                    csrfmiddlewaretoken: csrftoken,
                },
                dataType: 'json',
                complete: function (data) {
                    //debugMsg(data);
                },
                success: function (data) {
                    if (link && link == "first") {
                        location.href = '{% url 'app:heating-circuits' %}'
                    } else if (link && link == "last"){
                        location.href = '{% url 'app:heating-circuits' %}' + '?page=last'
                    }
                },
            });
        }

        $('#add_circuit').click(function(){
            addDeleteHc("add", "", "last")
        });

        $('.btn-delete-circuit').click(function(){
            var db_id = $(this).attr('db-id');
            addDeleteHc("delete", db_id, "first")
        });

        $('a').click(function () {
            saveCircuit()
        });
    </script>
{% endblock customscripts %}