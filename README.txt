A) First install
1. sudo apt-get install python3 python3-pip
2. pip3 install Django==1.10.5
3. sudo python3 manage.py makemigrations
4. sudo python3 manage.py migrate
5. sudo python3 manage.py createsuperuser
6. make sure hwd_info.txt file is in pipc/dev
7. sudo python3 manage.py runserver 0.0.0.0:8000 --insecure
# add --insecure if debug = False so that django can serve staticfiles.

B) On database update
1. sudo python3 manage.py makemigrations
2. sudo python3 manage.py migrate
3. sudo python3 manage.py runserver 0.0.0.0:8000 --insecure

C) Hardcoded PT values
home.html

