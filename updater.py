#!/usr/bin/python3
import json
import os
import ssl
import sys
import shutil
import zipfile
from urllib.request import urlopen, Request


def revertGui(gui_path, tempath):
    if os.path.exists(gui_path):
        shutil.rmtree(gui_path)
    os.rename(tempath, gui_path)
    os.chdir(gui_path)
    mkm = os.system('sudo python3 manage.py makemigrations')
    if mkm == 0:
        print("Migrations were applied.")
    migr = os.system('sudo python3 manage.py migrate')
    if migr == 0:
        print("Database was successfully migrated.")
    rserv = os.system('sudo python3 manage.py runserver 0.0.0.0:8000 --insecure &')
    if rserv == 0:
        print("Server is now running")


BASE_DIR = os.getcwd()
print(BASE_DIR)

gui_path = os.path.join(BASE_DIR, 'pipcwebmod')
gui_downloaded = os.path.join(BASE_DIR, 'gui_tmp')
tempath = os.path.join(BASE_DIR, 'temp_folder')

shutil.move(gui_path, tempath)
print("Gui moved to temporary folder.")

os.chdir(BASE_DIR)

os.rename(gui_downloaded, gui_path)
print("Downloaded gui folder renamed to 'pipcwebmod'")

pykill = os.system("sudo killall -9 python3")
if pykill == 0:
    print("All python processes were killed.")
else:
    print("Couldn't kill all the python processes.")

os.system("sudo chmod 777 -R " + gui_path)

db_path = os.path.join(tempath, 'pipconf.sqlite3')
try:
    shutil.copy2(db_path, gui_path)
    print("Database moved from temporary folder to project.")
except:
    print("Database couldnt move!")

os.chdir(gui_path)

mkm = os.system('sudo python3 manage.py makemigrations')
if mkm == 0:
    print("STEP1")
migr = os.system('sudo python3 manage.py migrate')
if migr == 0:
    print("STEP2")
rserv = os.system('sudo python3 manage.py runserver 0.0.0.0:8000 --insecure &')
if rserv == 0:
    print("STEP3")
    if os.path.exists(tempath):
        shutil.rmtree(tempath)
else:
    revertGui(gui_path, tempath)
    sys.exit()
